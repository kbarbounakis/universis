import { AfterViewInit, Component, Input, OnChanges, OnDestroy, SimpleChanges, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AngularDataContext } from '@themost/angular';
import { ErrorService, LoadingService } from '@universis/common';
import { AdvancedFormsService, ServiceUrlPreProcessor } from '@universis/forms';
import { BehaviorSubject, Observable, from } from 'rxjs';
import { first, flatMap } from 'rxjs/operators';

@Component({
  selector: 'one-roster-indivisible-class',
  template: `
<div class="card mb-0 shadow-none">
    <div class="card-body py-2">
    <div class="d-flex">
        <div>
        <h3 id=IndivisibleCourseClass class="card-title font-weight-normal" [translate]="'OneRoster.IndivisibleCourseClass'"></h3>
        <p class="card-text" innerHTML="{{ 'OneRoster.IndivisibleCourseClassDescription' | translate }}">
        </p>
        <div>
            <div *ngIf="courseClass && courseClass.sections.length === 0">
              <h5 class="text-success font-weight-normal" [translate]="'OneRoster.NoClassSectionsMessage'"></h5>
            </div>
            <ng-container *ngIf="courseClass && courseClass.sections.length > 0">
              <div class="mr-auto" *ngIf="(indivisible$ | async) as indivisible">
                <div class="my-3">
                    <input (change)="toggleSectionActive(indivisible.letSectionActive)" [(ngModel)]="indivisible.letSectionActive" name="checkbox-let-section-active" id="checkbox-let-section-active" class="checkbox checkbox-success" type="checkbox" />
                    <label for="checkbox-let-section-active" [translate]="'OneRoster.LetSectionActive'"></label>
                </div>
                  <button [disabled]="!(courseClass?.sections?.length > 0)" class="btn btn-outline-success" (click)="toggleIndivisible({ letSectionActive: indivisible.letSectionActive })">
                      <ng-container [ngSwitch]="indivisible.value">
                        <ng-container *ngSwitchCase="true">
                          {{ 'OneRoster.RemoveIndivisibilityStatus' | translate }}
                        </ng-container>
                        <ng-container *ngSwitchDefault>
                          {{ 'OneRoster.MarkClassAsIndivisible' | translate }}
                        </ng-container>
                      </ng-container>
                  </button>
                </div>
            </ng-container>
        </div>
    </div>
    </div>
</div>
  `,
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./one-roster-class-configuration.component.scss']
})
export class OneRosterIndivisibleClassComponent implements OnChanges, AfterViewInit, OnDestroy {

  @Input('courseClass') courseClass: { id: string, title: string, department: string, displayCode: string, year: string, period: string, sections: any[] };

  public indivisible$ = new BehaviorSubject<any>({
    value: false
  });

  private get indivisible$$(): Observable<any> {
    return from(
      this.context.model('OneRosterIndivisibleClasses').where('courseClass').equal(this.courseClass.id).select('indivisible', 'letSectionActive').getItem()
    ).map((result: any) => {
        if (result) {
          return {
            value: result.indivisible,
            letSectionActive: typeof result.letSectionActive === 'boolean' ? result.letSectionActive : false
          };
        }
        return {
          value: false,
          letSectionActive: false
        };
      });
  }

  constructor(
    private context: AngularDataContext,
    private activatedRoute: ActivatedRoute, 
    private errorService: ErrorService,
    private loadingService: LoadingService,
    private formService: AdvancedFormsService,
    private _translateService: TranslateService
  ) { }
  ngOnChanges(changes: SimpleChanges): void {
    if (changes.courseClass && changes.courseClass.currentValue) {
        this.indivisible$$.subscribe((indivisible) => {
            this.indivisible$.next(indivisible);
        });
    }
  }
  ngOnDestroy(): void {
    //
  }
  ngAfterViewInit(): void {
    
  }

  toggleSectionActive(value: boolean) {
    this.loadingService.showLoading();
    setTimeout(() => {
      this.context.model('OneRosterIndivisibleClasses').save({
        courseClass: this.courseClass.id,
        letSectionActive: value
      }).then(() => {
        this.loadingService.hideLoading();
      }).catch((err) => {
        this.errorService.showError(err, {
          continueLink: '.'
        });
      });
    }, 250);
  }

  toggleIndivisible(extras: { letSectionActive: boolean }) {
    this.loadingService.showLoading();
    this.indivisible$$.pipe(first()).subscribe(({value}) => {
      const setValue = !value;
      const item = {
        courseClass: this.courseClass.id,
        indivisible: setValue,
        letSectionActive:setValue ? extras.letSectionActive : false
      }
      this.context.model('OneRosterIndivisibleClasses').save(item).then(() => {
        this.loadingService.hideLoading();
        this.indivisible$.next({
          value: item.indivisible,
          letSectionActive: item.letSectionActive
        });
      }).catch((err) => {
        this.errorService.showError(err, {
          continueLink: '.'
        });
      });
    });    
  }

}