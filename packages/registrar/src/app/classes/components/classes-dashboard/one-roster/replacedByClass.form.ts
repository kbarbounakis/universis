const replacedByClassForm = {
    "model": "CourseClasses",
    "settings": {
      "i18n": {
        "el": {
          "Save": "Αποθήκευση"
        },
        "en": {
        }
      }
    },
    "components": [
      {
        "type": "fieldset",
        "components": [
          {
            "columns": [
              {
                "components": [
                  {
                    "hideLabel": true,
                    "labelPosition": "top",
                    "widget": "choicesjs",
                    "dataSrc": "url",
                    "customConditional": "show = data.courseClass != null",
                    "data": {
                      "url": "/CourseClasses?$top={{limit}}&$skip={{skip}}&$select=id,course/displayCode as displayCode,title&$filter=(indexof(title, '{{encodeURIComponent(search||'')}}') ge 0) and department eq '{{data.courseClass.department}}' and year eq {{data.courseClass.year}} and period eq {{data.courseClass.period}} and id ne '{{data.courseClass.id}}'&$orderby=title",
                      "headers": []
                    },
                    "validate": {
                      "required": false
                    },
                    "limit": 50,
                    "searchField": "search",
                    "searchEnabled": true,
                    "idProperty": "id",
                    "lazyLoad": false,
                    "multiple": false,
                    "dataType": "object",
                    "template": "({{ item.displayCode }}) {{ item.title }}",
                    "selectThreshold": 0.3,
                    "key": "replacedBy",
                    "type": "select",
                    "selectValues": "value",
                    "input": true
                  }
                ],
                "width": 8
              }
            ],
            "customClass": "text-dark",
            "hideLabel": true,
            "type": "columns",
            "input": false
          },
          {
            "label": "Save",
            "action": "event",
            "showValidations": false,
            "theme": "theme",
            "tableView": false,
            "key": "save",
            "type": "button",
            "event": "replace",
            "input": true
          }
        ]
      }
    ],
    "width": 3
  }
  
  export {
    replacedByClassForm
  }