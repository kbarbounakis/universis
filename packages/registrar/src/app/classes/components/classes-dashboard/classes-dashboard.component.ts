import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DiagnosticsService, ErrorService } from '@universis/common';
import { Observable, from } from 'rxjs';

@Component({
  selector: 'app-classes-dashboard',
  templateUrl: './classes-dashboard.component.html',
  styleUrls: ['./classes-dashboard.component.scss']
})
export class ClassesDashboardComponent implements OnInit {

  public model: any;
  public tabs: any[];
  public oneRosterEnabled$: Observable<boolean> = from(this._diagnosticsService.hasService('OneRosterService'));
  constructor(private _activatedRoute: ActivatedRoute, 
    private _diagnosticsService: DiagnosticsService, 
    private _errorService: ErrorService) { }

  ngOnInit() {

    this._diagnosticsService.hasService('EudoxusService').then((result) => {
        this.tabs = this._activatedRoute.routeConfig.children.filter((route) => {
          if (route.data && route.data.hidden) {
            return false;
          }
          return true;
        }).filter( route => typeof route.redirectTo === 'undefined').filter(el => result === false? el.path !== 'books': el);
    }).catch((err) => {
      this._errorService.showError(err, {
        continueLink: '.'
      });
    });

  }

}