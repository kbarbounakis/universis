import {Component, OnInit, ViewChild, Input, OnDestroy, Output, EventEmitter} from '@angular/core';
import {
  AdvancedTableComponent,
  AdvancedTableDataResult,
  ActivatedTableService,
  AdvancedSearchFormComponent,
  AdvancedTableSearchComponent,
  AdvancedRowActionComponent,
  TableColumnConfiguration
} from '@universis/ngx-tables';
import {Observable, Subscription} from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import {DIALOG_BUTTONS, ErrorService, LoadingService, ModalService} from '@universis/common';
import {ClientDataQueryable} from '@themost/client';
import {AngularDataContext} from '@themost/angular';
import { TranslateService } from '@ngx-translate/core';
import { COURSE_SHARED_COLUMNS as courseSharedColumns } from '../../../courses/components/courses-table/courses-shared-columns-config';

@Component({
  selector: 'app-classes-table',
  templateUrl: './classes-table.component.html',
  styles: []
})
export class ClassesTableComponent implements OnInit, OnDestroy  {

  public recordsTotal: any;
  public description; any;
  private dataSubscription: Subscription;
  private paramSubscription: Subscription;
  private selectedItems = [];
  @Input() tableConfiguration: any;
  @Input() searchConfiguration: any;
  @ViewChild('table') table: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  @ViewChild('advancedSearch') advancedSearch: AdvancedTableSearchComponent;
  @Output() refreshAction: EventEmitter<any> = new EventEmitter<any>();

  private fromYear: any;
  private fromPeriod: any;
  private copyRulesData = null;
  constructor(private _activatedRoute: ActivatedRoute,
              private _activatedTable: ActivatedTableService,
              private _modalService: ModalService,
              private _loadingService: LoadingService,
              private _errorService: ErrorService,
              private _context: AngularDataContext,
              private _translateService: TranslateService) {
  }

  ngOnInit() {
    this.dataSubscription = this._activatedRoute.data.subscribe(data => {
      this.recordsTotal = 0;
      this._activatedTable.activeTable = this.table;
      // set search form
      if (data.searchConfiguration) {
        this.search.form = data.searchConfiguration;
        this.search.ngOnInit();
      }
      // set table config and recall data
      if (data.tableConfiguration) {
        this.description = data.tableConfiguration.description;
        // set config
        this.table.config = data.tableConfiguration;
        // reset search text
        this.advancedSearch.text = null;
        // reset table
        this.table.reset(false);
      }
    });
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }
  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
  }

  async getSelectedItems() {
    let items = [];
    if (this.table && this.table.lastQuery) {
      const lastQuery: ClientDataQueryable = this.table.lastQuery;
      if (lastQuery != null) {
        if (this.table.smartSelect) {
          // get items
          const selectArguments = ['id', 'status/alternateName as status', 'year/id as year', 'period',
            'course/id as course', 'numberOfStudents', 'statistic/registrationType as registrationType',
            'weekHours', 'totalHours'];
          // query items
          const queryItems = await lastQuery.select.apply(lastQuery, selectArguments)
              .take(-1)
              .skip(0)
              .getItems();
          if (this.table.unselected && this.table.unselected.length) {
            // remove items that have been unselected by the user
            items = queryItems.filter( item => {
              return this.table.unselected.findIndex( (x) => {
                return x.id === item.id;
              }) < 0;
            });
          } else {
            items = queryItems;
          }
        } else {
          // get selected items only
          items = this.table.selected.map( (item) => {
            return {
              id: item.id,
              status: item.status,
              year: item.year,
              period: item.academicPeriod,
              course: item.course,
              numberOfStudents: item.numberOfStudents,
              registrationType: item.registrationType,
              weekHours: item.weekHours,
              totalHours: item.totalHours
            };
          });
        }
      }
    }
    return items;
  }

  /**
   * Opens selected course classes
   */
  async openAction() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      // get only items that may be opened
      this.selectedItems = items.filter( (item) => {
        return item.status !== 'open';
      });
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Classes.OpenAction.Title',
          description: 'Classes.OpenAction.Description',
          refresh: this.refreshAction,
          execute: this.executeChangeStatusAction('open')
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  /**
   * Copies selected course classes
   */
  async copyAction() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      // copy can be made only for course classes that belong to same year and period
      let fromYear =  [...items.map(item => item.year)];
      fromYear = fromYear.filter( function( item, index, inputArray ) {
        return inputArray.indexOf(item) === index;
      });
      let fromPeriod =  [...items.map(item => item.period)];
      fromPeriod = fromPeriod.filter( function( item, index, inputArray ) {
        return inputArray.indexOf(item) === index;
      });
      if (fromYear.length !== 1 || fromPeriod.length !== 1) {
        this.selectedItems = [];
      } else {
        this.selectedItems = items;
        this.fromYear = fromYear;
        this.fromPeriod = fromPeriod;
      }
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          formTemplate: this.selectedItems && this.selectedItems.length > 0 ? 'CourseClasses/copy' : null,
          modalTitle: 'Classes.CopyAction.Title',
          description: 'Classes.CopyAction.Description',
          errorMessage: 'Classes.CopyAction.CompletedWithErrors',
          refresh: this.refreshAction,
          execute: this.executeCopyAction()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  /**
   * Create exams for selected course classes
   */
  async createExams() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      // create exams can be made only for course classes that belong to same year and period
      let fromYear =  [...items.map(item => item.year)];
      fromYear = fromYear.filter( function( item, index, inputArray ) {
        return inputArray.indexOf(item) === index;
      });
      let fromPeriod =  [...items.map(item => item.period)];
      fromPeriod = fromPeriod.filter( function( item, index, inputArray ) {
        return inputArray.indexOf(item) === index;
      });
      if (fromYear.length !== 1 || fromPeriod.length !== 1) {
        this.selectedItems = [];
      } else {
        this.selectedItems = items;
        this.fromYear = fromYear;
        this.fromPeriod = fromPeriod;
      }
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          data: {'year': this.fromYear[0], 'period':  this.fromPeriod[0]},
          formTemplate: this.selectedItems && this.selectedItems.length > 0 ? 'CourseClasses/createExams' : null,
          modalTitle: 'Classes.CreateExamsAction.Title',
          description: 'Classes.CreateExamsAction.Description',
          errorMessage: 'Classes.CreateExamsAction.CompletedWithErrors',
          refresh: this.refreshAction,
          execute: this.executeCreateExamAction()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }
  /**
   * Executes open action for course classes
   */
  executeChangeStatusAction(status) {
    return new Observable((observer) => {
      this.refreshAction.emit({
        progress: 1
      });
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      // execute update for all items (transactional update)
      // map items
      const updated = this.selectedItems.map((item) => {
        return {
          id: item.id,
          status: {
            alternateName: status
          }
        };
      });
      // handle fake progress with interval
      let progressValue = 5;
      const progressInterval = setInterval(() => {
        progressValue = progressValue + 10 < 100 ? progressValue + 10 : 5;
        this.refreshAction.emit({
          progress: progressValue
        });
      }, 1000);
      this._context.model('CourseClasses').save(updated).then(() => {
        // reload table
        this.table.fetch(true);
        // stop progress
        if (progressInterval) {
          clearInterval(progressInterval);
        }
        // and return result
        return observer.next(result);
      }).catch((err) => {
        // stop progress
        if (progressInterval) {
          clearInterval(progressInterval);
        }
        // and return result with errors
        result.errors = result.total;
        return observer.next(result);
      });
    });
  }

  /**
   * Executes copy action for course classes
   */
  executeCopyAction() {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      // get values from modal component
      const component = this._modalService.modalRef.content as AdvancedRowActionComponent;
      const data = component.formComponent.form.formio.data;
      if (data.year == null || data.year == null || data.year === '' || data.period === '') {
        this.selectedItems = [];
        result.errors = result.total;
        return observer.next(result);
      }
      this.refreshAction.emit({
        progress: 1
      });
      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            const exists =  await this._context.model('CourseClasses')
              .where('course').equal(item.course)
              .and('year').equal(data.year)
              .and('period').equal(data.period).getItem();
            if (!exists) {
              await this._context.model(`CourseClasses/${encodeURIComponent(item.id)}/copy`).save(data);
              result.success += 1;
            } else {
              result.errors += 1;
            }
            // do not throw error while updating row
            // (user may refresh view)
          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }
        }
        try {
          this.table.fetch(true);
        } catch (err) {
          //
        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  /**
   * Executes Create Exam action for course classes
   */
  executeCreateExamAction() {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      // get values from modal component
      const component = this._modalService.modalRef.content as AdvancedRowActionComponent;
      const data = component.formComponent.form.formio.data;
      if (data.year == null || data.year == null || data.year === '' || data.examPeriod === '') {
        this.selectedItems = [];
        result.errors = result.total;
        return observer.next(result);
      }
      this.refreshAction.emit({
        progress: 1
      });
      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
              await this._context.model(`CourseClasses/${encodeURIComponent(item.id)}/createExam`).save(data);
              result.success += 1;
             // do not throw error while updating row
            // (user may refresh view)
            try {
              await this.table.fetchOne({
                id: item.id
              });
            } catch (err) {
              //
            }
          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }
        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  /**
   * Closes selected course classes
   */
  async closeAction() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      // get only items that may be opened
      this.selectedItems = items.filter( (item) => {
        return item.status !== 'closed';
      });
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Classes.CloseAction.Title',
          description: 'Classes.CloseAction.Description',
          refresh: this.refreshAction,
          execute: this.executeChangeStatusAction('closed')
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  /**
   * Deletes selected course classes
   */
  async deleteAction() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      // get only items without students
      this.selectedItems = items.filter( (item) => {
        return item.numberOfStudents === 0;
      });
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Classes.DeleteAction.Title',
          description: 'Classes.DeleteAction.Description',
          refresh: this.refreshAction,
          execute: this.executeDeleteAction()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }
  /**
   * Executes copy action for course classes
   */
  executeDeleteAction() {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      // get values from modal component
      this.refreshAction.emit({
        progress: 1
      });
      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            await this._context.model(`CourseClasses`).remove(item);
            result.success += 1;
          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }
        }
        try {
          await this.table.fetch(true);
        } catch (err) {
          //
        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  async copyRules() {
    try {
      // Only one row can be copied
      if (this.table.selected.length === 1) {
        const courseClassId = this.table.selected[0].id;

        this._context.model('CourseClasses')
          .where('id').equal(courseClassId)
          .getItem().then(async (courseClassesObject) => {
            if (courseClassesObject) {
              this._loadingService.showLoading();
              const courseClassRules = await this._context.model(`CourseClasses/${encodeURIComponent(courseClassesObject.id)}/RegistrationRules`).getItems();
              if (courseClassRules.length === 0) {
                const title = this._translateService.instant('Classes.CopyRules');
                const message = this._translateService.instant('Classes.CopyRulesAdditionalMessage');
                this._modalService.showDialog(title, message, DIALOG_BUTTONS.Ok).then(dialogResult => {
                  return;
                });
              } else {
                this.copyRulesData = {
                    source: courseClassesObject.id,
                    additionalType: courseClassRules[0].additionalType,
                    targetType: courseClassRules[0].targetType
                  };
              }
              this._loadingService.hideLoading();
            } else { // courseClassesObject is null - show error modal
              const title = this._translateService.instant('Classes.CopyRulesError');
              const message = this._translateService.instant('Classes.CopyRulesErrorMessage');
              this._modalService.showDialog(title, message, DIALOG_BUTTONS.Ok).then(dialogResult => {
                return;
              });
            }

          });
        this.table.selectNone();
      }
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }

  }

  async pasteRules() {
    try {
      // Show confirmation modal first
      const title = this._translateService.instant('Classes.PasteRules');
      const message = this._translateService.instant('Classes.PasteRulesAdditionalMessage');
      this._modalService.showDialog(title, message, DIALOG_BUTTONS.YesNo).then(async dialogResult => {
        if (dialogResult === 'no') {
          this.table.selectNone();
          return;
        }
        this._loadingService.showLoading();
        await Promise.all(this.table.selected.map(async courseClass => {
          const courseClassId = courseClass.id;

          return this._context.model('CourseClasses')
            .where('id').equal(courseClassId)
            .getItem().then(async (courseClassesObject) => {
              if (courseClassesObject) {
                if (this.copyRulesData.source !== courseClassesObject.id) {
                  this.copyRulesData.destination = courseClassesObject.id;
                  await this._context.model(`Rules/copyRules`).save(this.copyRulesData);
                }
                return true;
              } else { // courseClassesObject is null - show error modal
                 this._modalService.showDialog(this._translateService.instant('Classes.PasteRulesError'),
                  this._translateService.instant('Classes.PasteRulesErrorMessage'), DIALOG_BUTTONS.Ok).then(result => {
                  return;
                });
                return false;
              }

            });
        }));
        this.table.selectNone();
        this._loadingService.hideLoading();
      });

    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

async editRegistrationType() {
  try{
    this._loadingService.showLoading();
    const items = await this.getSelectedItems();
    this.selectedItems = items;
    this._modalService.openModalComponent(AdvancedRowActionComponent, {
      class: 'modal-lg',
      keyboard: false,
      ignoreBackdropClick: true,
      initialState: {
        items: this.selectedItems,
        formTemplate:'CourseClasses/editRegistrationType',
        modalTitle: 'Classes.EditRegistrationType.Title',
        description:'Classes.EditRegistrationType.Description',
        refresh: this.refreshAction,
        execute: this.executeEditRegistrationTypeAction()
      }
    })
  } catch (err) {
    this._errorService.showError(err, {
      continueLink: '.'
    });
  } finally {
    this._loadingService.hideLoading();
  }
}

executeEditRegistrationTypeAction(){
  return new Observable((observer) => {
    const total = this.selectedItems.length;
    const result = {
      total: this.selectedItems.length,
      success: 0,
      errors: 0
    };
    // get values from modal component
    const component = this._modalService.modalRef.content as AdvancedRowActionComponent;
    const data = component.formComponent.form.formio.data;
    if (!data.registrationType || !data.registrationType.hasOwnProperty('id')) {
      this.selectedItems = [];
      result.errors = result.total;
      return observer.next(result);
    }
    this.refreshAction.emit({
      progress: 1
    });
    // execute promises in series within an async method
    (async () => {
      for (let index = 0; index < this.selectedItems.length; index++) {
        try {
          const item = this.selectedItems[index];
          // set progress
          this.refreshAction.emit({
            progress: Math.floor(((index + 1) / total) * 100)
          });

          // save only CourseClasses whose registrationType is changed
          if (item.registrationType !== data.registrationType.id) {
            data.id = item.id;
            data.statistic = {
              'registrationType': data.registrationType
            };
            await this._context.model('CourseClasses').save(data);
            result.success += 1;
            try {
              await this.table.fetchOne({
                id: item.id
              });
            } catch (err) {
              console.log(err);
            }
          }
        } catch (err) {
          // log error
          console.log(err);
          result.errors += 1;
        }
      }
    })().then(() => {
      observer.next(result);
    }).catch((err) => {
      observer.error(err);
    });
  });
}

  /**
   * Calculates student statistics for selected course classes
   */
  async calculateStatistics() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      this.selectedItems = items;
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Classes.CalculateStatisticsAction.Title',
          description: 'Classes.CalculateStatisticsAction.Description',
          refresh: this.refreshAction,
          execute: this.executeCalculateStatisticsAction()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }
  /**
   * Executes calculate statistics action for course classes
   */
  executeCalculateStatisticsAction() {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
        this.refreshAction.emit({
        progress: 1
      });
      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            await this._context.model(`CourseClasses/${encodeURIComponent(item.id)}/calculateStatistics`).save(null);
            result.success += 1;
            // do not throw error while updating row
            // (user may refresh view)
            try {
              await this.table.fetchOne({
                id: item.id
              });
            } catch (err) {
              //
            }
          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }
        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  onLoading(event: { target: AdvancedTableComponent }) {
    if (event && event.target && event.target.config) {
      // clone shared columns
      const clonedSharedColumns = JSON.parse(JSON.stringify(courseSharedColumns));
      const configColumns = event.target.config.columns || [];
      clonedSharedColumns.forEach((column: TableColumnConfiguration) => {
        column.name = `course/${column.name}`;
        // ensure shared config's properies start with model's name
        if (!/^course/.test(column.property)){
          column.property = `course{column.property[0].toUpperCase() + column.property.slice(1)}`;
        }
        // try to find column in the config by name
        const findColumn = configColumns.find((configColumn) => configColumn.name === column.name);
        // if it does not exist
        if (findColumn == null) {
          // push it
          configColumns.push(column);
          // add column name to groupBy string if there is one and the column is not virtual
          if (event.target.config.defaults && event.target.config.defaults.groupBy && column.virtual !== true) {
            event.target.config.defaults.groupBy += `,${column.name}`;
          }
        }
      });
    }
  }
}
