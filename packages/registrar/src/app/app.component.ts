import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import {ConfigurationService, ReferrerRouteService} from '@universis/common';

@Component({
  selector: 'app-root',
  template: `<router-outlet></router-outlet>`
})
export class AppComponent implements OnInit{
  
  public constructor(private _titleService: Title,
    private _config: ConfigurationService, private referrer: ReferrerRouteService) { }
    
  ngOnInit() {
    const appSettings: { title: string } = this._config.settings.app as { title: string };
    const title = appSettings && appSettings.title; 
    // set application title
    this._titleService.setTitle( title );
  }
  
}
