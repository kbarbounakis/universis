import {Component, EventEmitter, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import * as DEPARTMENT_SNAPSHOTS_CONFIG from './department-snapshots.config.list.json';
import {AppEventService, DIALOG_BUTTONS, ErrorService, ModalService, ToastService} from '@universis/common';
import {TranslateService} from '@ngx-translate/core';
import {Subscription} from 'rxjs';
import {AdvancedSearchFormComponent} from '@universis/ngx-tables';
import {ActivatedTableService} from '@universis/ngx-tables';
import {
  AdvancedTableComponent,
  AdvancedTableConfiguration,
  AdvancedTableDataResult
} from '@universis/ngx-tables';
import {ActiveDepartmentService} from '../../../registrar-shared/services/activeDepartmentService.service';
import {CreateSnapshotComponent} from './create-snapshot.component';

@Component({
  selector: 'app-department-snapshots',
  templateUrl: './department-snapshots.component.html'
})
export class DepartmentSnapshotsComponent implements OnInit, OnDestroy {

  public readonly config: AdvancedTableConfiguration = <AdvancedTableConfiguration>DEPARTMENT_SNAPSHOTS_CONFIG;
  public recordsTotal: any;
  private dataSubscription: Subscription;
  @ViewChild('snapshots') snapshots: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  departmentId: any;
  private eventSubscription: Subscription;
  @Input() reload: EventEmitter<any> = new EventEmitter<any>();
  @Input() tableConfiguration: any;
  @Input() searchConfiguration: any;
  private subscription: Subscription;


  constructor(private _activatedRoute: ActivatedRoute,
              private _translateService: TranslateService,
              private _activatedTable: ActivatedTableService,
              private _errorService: ErrorService,
              private _modalService: ModalService,
              private _toastService: ToastService,
              private _context: AngularDataContext,
              private _activeDepartmentService: ActiveDepartmentService,
              private _appEvent: AppEventService
  ) {
  }

  async ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.departmentId = params.id;
      this._activatedTable.activeTable = this.snapshots;

      this.snapshots.query = this._context.model('DepartmentSnapshots')
        .where('department').equal(this.departmentId)
        .prepare();

      this.snapshots.config = AdvancedTableConfiguration.cast(DEPARTMENT_SNAPSHOTS_CONFIG);
      this.snapshots.fetch();

      this.eventSubscription = this._appEvent.changed.subscribe(change => {
        if (change && change.model === 'DepartmentSnapshots') {
          this.snapshots.fetch(true);
        }
      });

      this.dataSubscription = this._activatedRoute.data.subscribe(data => {
        if (data.tableConfiguration) {
          this.snapshots.config = data.tableConfiguration;
          this.snapshots.ngOnInit();
        }
      });
    });
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;

  }

  ngOnDestroy(): void {
    if (this.eventSubscription) {
      this.eventSubscription.unsubscribe();
    }

    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }

  /**
   * Create department snapshot
   */
  async createSnapshot() {
    try {
      // get department
      const activeDepartment = await this._context.model('LocalDepartments').where('id').equal(this.departmentId)
        .expand('locales', 'organization').getItem();
      activeDepartment.department = activeDepartment.id;
      delete activeDepartment.id;
      this._modalService.openModalComponent(CreateSnapshotComponent, {
        class: 'modal-lg',
        ignoreBackdropClick: true,
        closeOnSubmit: true,
        initialState: {
          data: activeDepartment
        }
      });
    } catch (err) {
      console.log(err);
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  remove() {
    if (this.snapshots && this.snapshots.selected && this.snapshots.selected.length) {
      const items = this.snapshots.selected;
      return this._modalService.showWarningDialog(
        this._translateService.instant('Snapshots.Remove.Title'),
        this._translateService.instant('Snapshots.Remove.Message'),
        DIALOG_BUTTONS.OkCancel).then(result => {
        if (result === 'ok') {
          this._context.model('DepartmentSnapshots').remove(items).then(() => {
            this._toastService.show(
              this._translateService.instant('Snapshots.Remove.Title'),
              this._translateService.instant('Settings.OperationCompleted')
            );
            this.snapshots.fetch(true);
          }).catch(err => {
            this._errorService.showError(err, {
              continueLink: '.'
            });
          });
        }
      });

    }
  }
}
