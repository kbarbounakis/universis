import {Component, Input, OnInit, OnDestroy, ViewChild, Output, EventEmitter} from '@angular/core';
import {
  AdvancedRowActionComponent,
  AdvancedTableComponent,
  AdvancedTableDataResult,
  AdvancedSearchFormComponent,
  AdvancedTableSearchComponent,
  ActivatedTableService,
  TableColumnConfiguration
} from '@universis/ngx-tables';
import { Router, ActivatedRoute } from '@angular/router';
import {Observable, Subscription} from 'rxjs';
import {UserActivityService, AppEventService, LoadingService, ErrorService, ModalService} from '@universis/common';
import { TranslateService } from '@ngx-translate/core';
import {ClientDataQueryable} from '@themost/client';
import {AngularDataContext} from '@themost/angular';

@Component({
  selector: 'app-validate-preview-results',
  templateUrl: './validate-preview-results.component.html'
})
export class ValidatePreviewResultsComponent implements OnInit, OnDestroy {

  private dataSubscription: Subscription;
  private paramSubscription: Subscription;

  @Input() tableConfiguration: any;
  @Input() searchConfiguration: any;

  @ViewChild('table') table: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  @ViewChild('advancedSearch') advancedSearch: AdvancedTableSearchComponent;
  @Output() refreshAction: EventEmitter<any> = new EventEmitter<any>();

  public recordsTotal: any;
  fragmentSubscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute,
              private _activatedTable: ActivatedTableService
  ) {
  }

  ngOnInit() {
    this.dataSubscription = this._activatedRoute.data.subscribe(data => {
      this._activatedTable.activeTable = this.table;
      // set search form
      if (data.searchConfiguration) {
        this.search.form = data.searchConfiguration;
        Object.assign(this.search.form, { department: this._activatedRoute.snapshot.data.department.id });
        this.search.ngOnInit();
      }
      // set table config and recall data
      if (data.tableConfiguration) {
        // set config
        this.table.config = data.tableConfiguration;
        // reset search text
        this.advancedSearch.text = null;
        // reset table
        this.table.reset(false);
      }
    });
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }
}
