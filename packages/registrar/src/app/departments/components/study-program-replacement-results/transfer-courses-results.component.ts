import {Component, EventEmitter, Input, OnInit, ViewChild, OnDestroy, Output} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import * as TRANSFER_COURSES_CONFIG from './transfer-courses-results-table.config.list.json';
import { Subject, Subscription} from 'rxjs';
import {AdvancedSearchFormComponent} from '@universis/ngx-tables';
import {ActivatedTableService} from '@universis/ngx-tables';
import {
  AdvancedTableComponent,
  AdvancedTableConfiguration,
  AdvancedTableDataResult
} from '@universis/ngx-tables';
import {ErrorService, LoadingService} from '@universis/common';
import {ClientDataQueryable} from '@themost/client';
import {TranslateService} from '@ngx-translate/core';
import {debounceTime, tap} from 'rxjs/operators';


@Component({
  selector: 'app-transfer-courses-results',
  templateUrl: './transfer-courses-results.component.html'
})
export class TransferCoursesResultsComponent implements OnInit, OnDestroy {

  public readonly config: AdvancedTableConfiguration = <AdvancedTableConfiguration>TRANSFER_COURSES_CONFIG;
  @ViewChild('students') students: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  public recordsTotal: any;
  private dataSubscription: Subscription;
  public refreshResultsList$ = new Subject();
  public inProgressAction: any;
  private fragmentSubscription: Subscription;
  @Input() reload: EventEmitter<any> = new EventEmitter<any>();
  private subscription: Subscription;
  @Output() refreshAction: EventEmitter<any> = new EventEmitter<any>();
  private department: any;
  private refreshSubscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute,
              private _activatedTable: ActivatedTableService,
              private _context: AngularDataContext,
              private _translateService: TranslateService,
              private loadingService: LoadingService,
              private errorService: ErrorService
  ) {
  }

  async ngOnInit() {
    this.dataSubscription = this._activatedRoute.data.subscribe(async (data) => {
      try {
        // get resolved active department
        this.department = this._activatedRoute.snapshot.data.department.id;
        this._activatedTable.activeTable = this.students;
        // set table query
        this.students.query = this._context
          .model('StudyProgramReplacementResults')
          .where('action/studyProgramReplacementRule/studyProgram/department')
          .equal(this.department)
          .orderByDescending('id')
          .prepare();
        this.students.config = AdvancedTableConfiguration.cast(TRANSFER_COURSES_CONFIG);
        this.students.fetch();
         if (data.searchConfiguration) {
          this.search.form =  Object.assign(data.searchConfiguration, {department: this.department});
          this.search.ngOnInit();
        }
        // get in progress action, if any
        this.inProgressAction = await this._context
          .model('StudyProgramReplacementActions')
          .where('studyProgramReplacementRule/studyProgram/department')
          .equal(this.department)
          .and('actionStatus/alternateName')
          .equal('ActiveActionStatus')
          .select('id', 'dateCreated')
          .getItem();

        this.refreshSubscription = this.refreshResultsList$
          .pipe(
            // show pseudo loading
            tap(() => this.loadingService.showLoading()),
            // allow activation once per second
            debounceTime(1000)
          )
          .subscribe(async () => {
            try {
              // refresh table
              if (this.students) {
                this.students.fetch();
              }
              // refresh in progress action
              this.inProgressAction = await this._context
                .model('StudyProgramReplacementResults')
                .where('action/studyProgramReplacementRule/studyProgram/department')
                .equal(this.department)
                .and('actionStatus/alternateName')
                .equal('ActiveActionStatus')
                .select('id', 'dateCreated')
                .getItem();
            } catch (err) {
              console.error(err);
            } finally {
              this.loadingService.hideLoading();
            }
          });
      } catch (err) {
        console.error(err);
        this.errorService.showError(err);
      }
    });
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
    if (this.refreshSubscription) {
      this.refreshSubscription.unsubscribe();
    }
  }

  async getSelectedItems() {
    let items = [];
    if (this.students && this.students.lastQuery) {
      const lastQuery: ClientDataQueryable = this.students.lastQuery;
      // search for document attributes (if table has published column)

      if (lastQuery != null) {
        if (this.students.smartSelect) {
          // get items
          const selectArguments = ['student/id as id'];

          // query items
          const queryItems = await lastQuery.select.apply(lastQuery, selectArguments)
            .take(-1)
            .skip(0)
            .getItems();
          if (this.students.unselected && this.students.unselected.length) {
            // remove items that have been unselected by the user
            items = queryItems.filter(item => {
              return this.students.unselected.findIndex((x) => {
                return x.id === item.id;
              }) < 0;
            });
          } else {
            items = queryItems;
          }
        } else {
          // get selected items only
          items = this.students.selected.map((item) => {
            return {
              id: item.id
            };
          });
        }
      }
    }
    return items;
  }

}
