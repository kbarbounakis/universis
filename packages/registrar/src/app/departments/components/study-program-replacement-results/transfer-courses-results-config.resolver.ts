import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {TableConfiguration} from '@universis/ngx-tables';

export class TransferCoursesTableConfigurationResolver implements Resolve<TableConfiguration> {
    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration {
        return import(`./transfer-courses-results-table.config.${route.params.list}.json`)
            .catch( err => {
           return  import(`packages/registrar/src/app/departments/components/study-program-replacement-results/transfer-courses-results-table.config.list.json`);
        });
    }
}


export class TransferCoursesDefaultTableConfigurationResolver implements Resolve<any> {
    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return import(`packages/registrar/src/app/departments/components/study-program-replacement-results/transfer-courses-results-table.search.list.json`);
    }
}
