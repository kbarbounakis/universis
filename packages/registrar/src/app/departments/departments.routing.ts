import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DepartmentsHomeComponent } from './components/departments-home/departments-home.component';
import { DepartmentsTableComponent } from './components/departments-table/departments-table.component';
import { DepartmentsRootComponent } from './components/departments-root/departments-root.component';
import { DepartmentsPreviewComponent } from './components/departments-preview/departments-preview.component';
// tslint:disable-next-line:max-line-length
import {AdvancedFormItemWithLocalesResolver, AdvancedFormRouterComponent} from '@universis/forms';
import {
  ActiveDepartmentResolver,
  CurrentAcademicPeriodResolver,
  CurrentAcademicYearResolver, LastStudyProgramResolver, ActiveDepartmentIDResolver
} from '../registrar-shared/services/activeDepartmentService.service';
import { AdvancedListComponent } from '@universis/ngx-tables';
// tslint:disable-next-line:max-line-length
import {AdvancedFormModalComponent, AdvancedFormModalData, AdvancedFormItemResolver, AdvancedFormResolver, AdvancedFormParentItemResolver} from '@universis/forms';
import * as DepartmentDocumentSeriesConfig from './documents-series.config.list.json';
import * as DepartmentDocumentSeriesEdit from './forms/DepartmentDocumentNumberSeries/edit.json';
import * as DepartmentDocumentSeriesNew from './forms/DepartmentDocumentNumberSeries/new.json';
import * as InstituteDocumentSeriesConfig from './institute-series.config.list.json';
import * as InstituteDocumentSeriesEdit from './forms/InstituteDocumentNumberSeries/edit.json';
import * as InstituteDocumentSeriesNew from './forms/InstituteDocumentNumberSeries/new.json';
// tslint:disable-next-line:max-line-length
import {DepartmentsTableConfigurationResolver, DepartmentsTableSearchResolver} from './components/departments-table/departments-table-config.resolver';
import {DepartmentsDashboardComponent} from './components/departements-dashboard/departments-dashboard.component';
// tslint:disable-next-line:max-line-length
import {DepartmentsDashboardOverviewComponent} from './components/departements-dashboard/departments-dashboard-overview/departments-dashboard-overview.component';
// tslint:disable-next-line:max-line-length
import {DepartmentsDashboardRegistrationsComponent} from './components/departements-dashboard/departments-dashboard-registrations/departments-dashboard-registrations.component';
import {ArchivedDocumentsHomeComponent} from './components/archived-documents-home/archived-documents-home.component';
import { ArchivedDocumentsTableComponent } from './components/archived-documents-table/archived-documents-table.component';
import { ArchivedDocumentsConfigurationResolver } from './components/archived-documents-table/archived-documents-table-config.resolver';
// tslint:disable-next-line:max-line-length
import { ArchivedDocumentsConfigurationSearchResolver } from './components/archived-documents-table/archived-documents-table-config.resolver';
import * as InstituteTableConfiguration from './institutes.config.list.json';
import * as InstituteSearchConfiguration from './institutes.search.list.json';
import * as DepartmentReportVariableValueConfiguration from './report-variables.config.json';
import {DepartmentSnapshotsComponent} from './components/departments-snapshots/department-snapshots.component';
// tslint:disable-next-line:max-line-length
import { DepartmentsPreviewUsersComponent } from './components/departments-preview/departments-preview-users/departments-preview-users.component';
import { DepartmentsAddUserComponent } from './components/departments-preview/departments-preview-users/departments-add-user.component';
import { DepartmentsSharedModule } from './departments.shared';
import { ActiveDepartmentSnapshotResolver, DepartmentSnapshotVariablesComponent } from './components/departments-snapshots/department-snapshot-variables.component';
import {ValidatePreviewResultsComponent} from './components/validate-preview-results/validate-preview-results.component';
import {
  ValidateResultsDefaultTableConfigurationResolver, ValidateResultsTableConfigurationResolver, ValidateResultsTableSearchResolver
} from './components/validate-preview-results/validate-preview-results-config.resolver';
import {GraduationValidationsHomeComponent} from './components/validate-preview-results/graduation-validattions-home/graduation-validations-home.component';
import { DepartmentDataAvailability } from './components/data-availability/data-availability.component';
import { InspectStudentDegreeComponent } from './components/data-availability/inspect-student-degree.component';
import {DeletedStudentsComponent} from './components/deleted-students/deleted-students.component';
import { AutoRegistrationsHomeComponent } from './components/auto-registration-results/auto-registrations-home/auto-registrations-home.component';
import { AutoRegistrationResultsComponent } from './components/auto-registration-results/auto-registration-results.component';
import { AutoRegistrationsMessageComponent } from './components/auto-registration-results/auto-registrations-message/auto-registrations-message.component';
import { DepartmentsPreviewPlacesComponent } from './components/departments-preview/departments-preview-places/departments-preview-places.component';
import { DepartmentsAddPlacesComponent } from './components/departments-preview/departments-preview-places/departments-add-places.component';
import {
  StudyProgramResultsHomeComponent
} from './components/study-program-replacement-results/study-program-results-home/study-program-results-home.component';
import {
  TransferCoursesDefaultTableConfigurationResolver,
} from './components/study-program-replacement-results/transfer-courses-results-config.resolver';
import {TransferCoursesResultsComponent} from './components/study-program-replacement-results/transfer-courses-results.component';
import {
  TransferCoursesMessageComponent
} from './components/study-program-replacement-results/transfer-courses-message/transfer-courses-message.component';
import {StudentsTableComponent} from '../students/components/students-table/students-table.component';
import {SearchConfigurationResolver, TableConfigurationResolver} from '../registrar-shared/table-configuration.resolvers';
import {RequestsHomeComponent} from '../requests/components/requests-home/requests-home.component';
import {RequestsTableComponent} from '../requests/components/requests-table/requests-table.component';
import { DepartmentRolesComponent } from './components/department-roles/department-roles.component';

const routes: Routes = [
    {
        path: '',
        component: DepartmentsHomeComponent,
        data: {
            title: 'Departments'
        },
        children: [
            {
            path: '',
            pathMatch: 'full',
            redirectTo: 'list/index'
          },
          {
            path: 'list',
            pathMatch: 'full',
            redirectTo: 'list/undergraduate'
          },
          {
            path: 'list/:list',
            component: DepartmentsTableComponent,
            data: {
              title: 'Departments List'
            },
            resolve: {
              tableConfiguration: DepartmentsTableConfigurationResolver,
              searchConfiguration: DepartmentsTableSearchResolver
            },

          }
        ]
    },
  {
    path: 'create',
    component: DepartmentsRootComponent,
    children: [
      {
        path: 'new',
        pathMatch: 'full',
        component: AdvancedFormRouterComponent,
        data: {
        },
        resolve: {
          department: ActiveDepartmentResolver,
          inscriptionYear: CurrentAcademicYearResolver,
          inscriptionPeriod: CurrentAcademicPeriodResolver,
          studyProgram: LastStudyProgramResolver
        }
      }
    ]
  },
    {
      path: 'new',
      component: AdvancedFormRouterComponent,
      resolve: {
        department: ActiveDepartmentResolver,
        inscriptionYear: CurrentAcademicYearResolver,
        inscriptionPeriod: CurrentAcademicPeriodResolver,
        studyProgram: LastStudyProgramResolver
      }
    },
    {
      path: 'current/documents',
      component: ArchivedDocumentsHomeComponent,
      resolve: {
        department: ActiveDepartmentResolver
      },
      children: [
        {
          path: 'series/:documentSeries/items/:list',
          component: ArchivedDocumentsTableComponent,
          resolve: {
            documents: ArchivedDocumentsConfigurationResolver,
            searchConfiguration: ArchivedDocumentsConfigurationSearchResolver
          }
        }]
    },

  {
    path: 'current/validation-results',
    component: GraduationValidationsHomeComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'list/graduation'
      },
      {
        path: 'list',
        pathMatch: 'full',
        redirectTo: 'list/graduation'
      },
      {
        path: 'list/:list',
        component: ValidatePreviewResultsComponent,
        resolve: {
          department: ActiveDepartmentResolver,
          tableConfiguration: ValidateResultsTableConfigurationResolver,
          searchConfiguration: ValidateResultsTableSearchResolver
        },
      }
    ]
  },
  {
    path: 'current/transfer-courses',
    component: StudyProgramResultsHomeComponent,
    resolve: {
      department: ActiveDepartmentResolver
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'results'
      },
      {
        path: 'results',
        component: TransferCoursesResultsComponent,
        resolve: {
          searchConfiguration: TransferCoursesDefaultTableConfigurationResolver
        },
        children: [
          {
            path: ':action/message',
            component: TransferCoursesMessageComponent,
            outlet: 'modal'
          }
        ]
      }
    ]
  },
  {
    path: 'current/automated-registrations',
    component: AutoRegistrationsHomeComponent,
    resolve: {
      department: ActiveDepartmentResolver
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'results'
      },
      {
        path: 'results',
        component: AutoRegistrationResultsComponent,
        children: [
          {
            path: ':action/message',
            component: AutoRegistrationsMessageComponent,
            outlet: 'modal'
          }
        ]
      }
    ]
  },
  {
    path: 'configuration/dataAvailability',
    component: DepartmentDataAvailability,
    children: [
      {
        path: ':id/edit',
        pathMatch: 'full',
        component: AdvancedFormModalComponent,
        outlet: 'modal',
        data: <AdvancedFormModalData>{
          model: 'LocalDepartments',
          action: 'editAvailability',
          closeOnSubmit: true,
          serviceQueryParams: {
            $select: 'id,name',
            $expand: 'dataAvailability'
          }
        },
        resolve: {
          formConfig: AdvancedFormResolver,
          data: AdvancedFormItemResolver
        }
      }
    ]
  },
  {
    path: 'configuration/dataAvailability/validate',
    component: InspectStudentDegreeComponent,
  },
  {
    path: 'configuration/institutes/series',
    component: AdvancedListComponent,
    data: {
      model: 'InstituteDocumentNumberSeries',
      tableConfiguration: InstituteDocumentSeriesConfig,
      category: 'Sidebar.Departments',
      description: 'Documents.Lists.InstituteDocumentNumberSeries.Description',
      longDescription: 'Documents.Lists.InstituteDocumentNumberSeries.LongDescription'
    },
    children: [
      {
        path: 'add',
        pathMatch: 'full',
        component: AdvancedFormModalComponent,
        outlet: 'modal',
        data: <AdvancedFormModalData>{
          action: 'new',
          closeOnSubmit: true,
          formConfig: InstituteDocumentSeriesNew
        }
      },
      {
        path: ':id/edit',
        pathMatch: 'full',
        component: AdvancedFormModalComponent,
        outlet: 'modal',
        data: <AdvancedFormModalData>{
          action: 'edit',
          closeOnSubmit: true,
          formConfig: InstituteDocumentSeriesEdit,
        },
        resolve: {
          data: AdvancedFormItemResolver
        }
      }
    ]
  },
  {
    path: 'configuration/institutes',
    component: AdvancedListComponent,
    data: {
      model: 'Institutes',
      tableConfiguration: InstituteTableConfiguration,
      searchConfiguration: InstituteSearchConfiguration,
      category: 'Institutes',
      description: 'Settings.Lists.Institute.Description',
      longDescription: 'Settings.Lists.Institute.LongDescription'
    },
    children: [
      {
        path: 'add',
        pathMatch: 'full',
        component: AdvancedFormModalComponent,
        outlet: 'modal',
        data: <AdvancedFormModalData>{
          action: 'new',
          closeOnSubmit: true
        },
        resolve: {
        }
      },
      {
        path: ':id/edit',
        pathMatch: 'full',
        component: AdvancedFormModalComponent,
        outlet: 'modal',
        data: <AdvancedFormModalData>{
          action: 'edit',
          serviceQueryParams: {
            $expand: 'registrationPeriods,locales,instituteConfiguration($expand=usernameFormatSource)'
          },
          closeOnSubmit: true,
        },
        resolve: {
          data: AdvancedFormItemWithLocalesResolver
        }
      }
    ]
  },
  {
    path: 'current/reportVariables',
    component: AdvancedListComponent,
    data: {
      model: 'DepartmentReportVariableValues',
      tableConfiguration: DepartmentReportVariableValueConfiguration,
      category: 'Departments',
      description: 'Settings.Lists.DepartmentReportVariableValue.Description',
      longDescription: 'Settings.Lists.DepartmentReportVariableValue.LongDescription'
    },
    children: [
      {
        path: 'add',
        pathMatch: 'full',
        component: AdvancedFormModalComponent,
        outlet: 'modal',
        data: <AdvancedFormModalData>{
          action: 'new',
          closeOnSubmit: true,
          description: null
        },
        resolve: {
          department: ActiveDepartmentIDResolver
        }
      },
      {
        path: ':id/edit',
        pathMatch: 'full',
        component: AdvancedFormModalComponent,
        outlet: 'modal',
        data: <AdvancedFormModalData>{
          action: 'edit',
          serviceQueryParams: {
          },
          closeOnSubmit: true,
        },
        resolve: {
          data: AdvancedFormItemResolver
        }
      }
    ]
  },
  {
    path: 'current/restore/students',
    component: DeletedStudentsComponent,
    resolve: {
      department: ActiveDepartmentResolver
    }
  },
    {
        path: 'current/documents/series',
        component: AdvancedListComponent,
        data: {
            model: 'DepartmentDocumentNumberSeries',
            tableConfiguration: DepartmentDocumentSeriesConfig,
            category: 'Sidebar.Departments',
            description: 'Documents.Lists.DepartmentDocumentNumberSeries.Description',
            longDescription: 'Documents.Lists.DepartmentDocumentNumberSeries.LongDescription'
        },
        children: [
            {
                path: 'add',
                pathMatch: 'full',
                component: AdvancedFormModalComponent,
                outlet: 'modal',
                data: <AdvancedFormModalData>{
                    action: 'new',
                    closeOnSubmit: true,
                    formConfig: DepartmentDocumentSeriesNew
                },
                resolve: {
                    department: ActiveDepartmentIDResolver
                }
            },
            {
                path: ':id/edit',
                pathMatch: 'full',
                component: AdvancedFormModalComponent,
                outlet: 'modal',
                data: <AdvancedFormModalData>{
                    action: 'edit',
                    closeOnSubmit: true,
                    formConfig: DepartmentDocumentSeriesEdit,
                    serviceQueryParams: {
                      $expand: 'parent'
                   },
                },
                resolve: {
                    data: AdvancedFormItemResolver,
                    department: ActiveDepartmentIDResolver
                }
            }
        ]
    },
    {
        path: ':id',
        component: DepartmentsRootComponent,
        data: {
            title: 'Departments Home'
        },
        children: [
            {
                path: '',
                pathMatch: 'full',
                redirectTo: 'dashboard'
            },
            {
                path: 'dashboard',
                component: DepartmentsDashboardComponent,
                data: {
                    title: 'Departments Dashboard'
                },
                children: [
                    {
                        path: '',
                        redirectTo: 'general'
                    },
                    {
                        path: 'general',
                        component: DepartmentsDashboardOverviewComponent,
                        data: {
                            title: 'Department\'s View'
                        }
                    },
                    {
                      path: 'history/:snapshot/variables',
                      component: DepartmentSnapshotVariablesComponent,
                      data: {
                        model: 'DepartmentReportVariableSnapshots',
                        description: 'Settings.Lists.DepartmentReportVariableValue.Description',
                        longDescription: 'Settings.Lists.DepartmentReportVariableValue.LongDescription'
                      },
                      children: [
                        {
                          path: 'add',
                          pathMatch: 'full',
                          component: AdvancedFormModalComponent,
                          outlet: 'modal',
                          data: <AdvancedFormModalData>{
                            action: 'new',
                            closeOnSubmit: true,
                            description: null
                          },
                          resolve: {
                            department: ActiveDepartmentSnapshotResolver
                          }
                        },
                        {
                          path: ':id/edit',
                          pathMatch: 'full',
                          component: AdvancedFormModalComponent,
                          outlet: 'modal',
                          data: <AdvancedFormModalData>{
                            action: 'edit',
                            serviceQueryParams: {
                            },
                            closeOnSubmit: true,
                          },
                          resolve: {
                            data: AdvancedFormItemResolver
                          }
                        }
                      ]
                    },
                  {
                    path: 'history',
                    component: DepartmentSnapshotsComponent,
                    children: [
                      {
                        path: ':id/edit',
                        pathMatch: 'full',
                        component: AdvancedFormModalComponent,
                        outlet: 'modal',
                        data: <AdvancedFormModalData>{
                          model: 'DepartmentSnapshots',
                          action: 'edit',
                          serviceQueryParams: {
                            $expand: 'locales, reportVariables'
                          },
                          closeOnSubmit: true
                        },
                        resolve: {
                          formConfig: AdvancedFormResolver,
                          data: AdvancedFormItemWithLocalesResolver
                        }
                      },
                    ]
                  },
                  {
                    path: 'users',
                    component: DepartmentsPreviewUsersComponent,
                    children: [
                      {
                        path: 'add',
                        pathMatch: 'full',
                        component: DepartmentsAddUserComponent,
                        outlet: 'modal',
                        data: <AdvancedFormModalData> {
                          title: 'Departments.AddUser',
                          config: DepartmentsSharedModule.UsersList
                        },
                        resolve: {
                          department: AdvancedFormParentItemResolver
                        }
                      }
                    ]
                  },
                  {
                    path: 'places',
                    component: DepartmentsPreviewPlacesComponent,
                    children: [
                      {
                        path: 'add',
                        pathMatch: 'full',
                        component: DepartmentsAddPlacesComponent,
                        outlet: 'modal',
                        data: <AdvancedFormModalData>{
                          title: 'Departments.AddPlace',
                          config: DepartmentsSharedModule.PlacesList
                        },
                        resolve: {
                          department: AdvancedFormParentItemResolver
                        }
                      }
                    ]
                  },
                  {
                    path: 'roles',
                    component: DepartmentRolesComponent,
                    children: [
                      {
                        path: ':id/edit',
                        pathMatch: 'full',
                        component: AdvancedFormModalComponent,
                        outlet: 'modal',
                        data: <AdvancedFormModalData>{
                          title: 'Departments.Roles.Edit.Title',
                          model: 'DepartmentRoles',
                          action: 'edit',
                          closeOnSubmit: true,
                          serviceQueryParams: {
                            $expand: 'department($select=id, name), personnel($expand=user, person)'
                          },
                        },
                        resolve: {
                          formConfig: AdvancedFormResolver,
                          data: AdvancedFormItemResolver
                        }
                      },
                    ]
                  },
                  {
                    path: 'registration',
                    component: DepartmentsDashboardRegistrationsComponent,
                    data: {
                      title: 'Theses Registrations Rules'
                    }
                  }
                ]
            },
            {
              path: 'edit',
              component: AdvancedFormRouterComponent,
              data: {
                action: 'edit',
                serviceQueryParams: {
                  $expand: 'locales'
                }
              },
              resolve: {
                data: AdvancedFormItemWithLocalesResolver
              }
            },
            {
                path: ':action',
                component: AdvancedFormRouterComponent
            }
        ]
    },
    {
      path: 'configuration/degree-templates',
      loadChildren: '../degree-templates/degree-templates.module#DegreeTemplatesModule'
    },
  {
    path: 'configuration/hahe-units',
    loadChildren: '../hahe-units/hahe-units.module#HaheUnitsModule'
  }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    declarations: []
})
export class DepartmentsRoutingModule {
}
