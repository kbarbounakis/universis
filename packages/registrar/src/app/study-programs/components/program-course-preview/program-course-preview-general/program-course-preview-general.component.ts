import { Component, OnInit } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-program-course-preview-general',
  templateUrl: './program-course-preview-general.component.html'
})
export class ProgramCoursePreviewGeneralComponent implements OnInit {
  public model: any;

  constructor(private _activatedRoute: ActivatedRoute,
    private _context: AngularDataContext) {
}

  async ngOnInit() {
    this.model = await this._context.model('StudyProgramCourses')
      .where('id').equal(this._activatedRoute.snapshot.params.CourseID)
      .expand('course')
      .getItem();
  }

  // .expand('department,studyLevel')

}
