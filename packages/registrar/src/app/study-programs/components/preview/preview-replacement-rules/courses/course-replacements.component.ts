import { Component, OnInit, ViewChild, Input, OnDestroy } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {
  ActivatedTableService,
  AdvancedTableComponent,
  AdvancedTableConfiguration,
  AdvancedTableDataResult
} from '@universis/ngx-tables';
import { AngularDataContext } from '@themost/angular';
import {Observable, Subscription} from 'rxjs';
import {TranslateService} from '@ngx-translate/core';
import {
  AppEventService,
  DIALOG_BUTTONS,
  ErrorService,
  LoadingService,
  ModalService,
  ToastService,
  UserActivityService
} from '@universis/common';
 import {AdvancedSearchFormComponent} from '@universis/ngx-tables';
 import {AddCoursesComponent} from '../../add-courses/add-courses.component';
import {TableConfigurationResolver} from '../../../../../registrar-shared/table-configuration.resolvers';

@Component({
  selector: 'app-course-replacements',
  templateUrl: './course-replacements.component.html'
})
export class CourseReplacementsComponent implements OnInit, OnDestroy {

  private dataSubscription: Subscription;
  private paramSubscription: Subscription;
  private fragmentSubscription: Subscription;
  private studyProgram: any;
  @Input() tableConfiguration: any;
  @Input() searchConfiguration: any;
  @ViewChild('courses') courses: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  public recordsTotal: any;
  private selectedItems = [];
  private courseReplacement: any;
  public copyRulesData = null;
  public studyProgramReplacementRule: any;
  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _translateService: TranslateService,
              private _userActivityService: UserActivityService,
              private _router: Router,
              private _errorService: ErrorService,
              private _modalService: ModalService,
              private _loadingService: LoadingService,
            private _toastService: ToastService,
            private _activatedTable: ActivatedTableService)  { }

  async ngOnInit() {
    this.dataSubscription = this._activatedRoute.data.subscribe(data => {
      // get query params
      if (this.paramSubscription) {
        this.paramSubscription.unsubscribe();
      }
      this.fragmentSubscription = this._activatedRoute.fragment.subscribe((fragment) => {
        if (fragment === 'reload') {
          this.courses.fetch();
        }
        // get activated table
        this._activatedTable.activeTable = this.courses;
      });
      this.paramSubscription = this._activatedRoute.params.subscribe((params) => {
        if (data.tableConfiguration) {
          this.studyProgram = parseInt(params.id, 10) || 0;
          const queryParams = this._context.model('CourseReplacements')
              .where('studyProgramReplacementRule/studyProgram').equal(this.studyProgram)
              .and('studyProgramReplacementRule').equal(params.rule)
              .getParams();
          this.courses.destroy();
          data.tableConfiguration.defaults.filter = queryParams.$filter;
          this.courses.config = AdvancedTableConfiguration.cast(data.tableConfiguration, true);
          this.courses.fetch(true);
        }
        if (data.searchConfiguration) {
          this.search.form = data.searchConfiguration;
          this.search.formComponent.formLoad.subscribe((res: any) => {
            Object.assign(res, {studyProgram: this._activatedRoute.snapshot.params.id, activeDepartment: data.department.id});
          });
          this.search.ngOnInit();
        }
        this._context.model('StudyProgramReplacementRules')
            .where('id').equal(params.rule)
            .getItem().then((item) => {
          if (item) {
            this.studyProgramReplacementRule = item;
          }
        });
      });
    });
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
  }



  remove() {
    if (this.courses && this.courses.selected && this.courses.selected.length) {
      // get items to remove
      const items = this.courses.selected.map(item => {
        return {
          id: item.id
        };
      });
      return this._modalService.showWarningDialog(
          this._translateService.instant('StudyPrograms.Replacements.RemoveCourseTitle'),
          this._translateService.instant('StudyPrograms.Replacements.RemoveCourseMessage'),
          DIALOG_BUTTONS.OkCancel).then(result => {
        if (result === 'ok') {
          this._context.model('CourseReplacements').remove(items).then(() => {
            this._toastService.show(
                this._translateService.instant('StudyPrograms.Replacements.RemoveCourseMessage'),
                this._translateService.instant((items.length === 1 ?
                        'StudyPrograms.Replacements.RemoveOne' : 'StudyPrograms.Replacements.RemoveMany')
                    , { value: items.length })
            );
            this.courses.fetch(true);
          }).catch(err => {
            this._errorService.showError(err, {
              continueLink: '.'
            });
          });
        }
      });
    }
  }




  async copy() {
    // Only one row can be copied
    if (this.courses.selected.length === 1) {

      const courseID = this.courses.selected[0].courseId;

      const programCourseObject = await this._context.model('StudyProgramCourses')
      .where('studyProgram').equal(this.studyProgram)
      .and('course').equal(courseID)
      .getItem();

        if (programCourseObject) { // If programCourse ID exists then get the rules
          this._loadingService.showLoading();
          const courseRules = await this._context.model(`StudyProgramCourses/${programCourseObject.id}/RegistrationRules`).getItems();
          if (courseRules.length === 0) {
            const title = this._translateService.instant('StudyPrograms.CopyRules');
            const message = this._translateService.instant('StudyPrograms.CopyRulesAdditionalMessage');
            this._modalService.showDialog(title, message, DIALOG_BUTTONS.Ok).then(dialogResult => {
              return;
            });
          } else {
            this.copyRulesData = {
              source: programCourseObject.id,
              additionalType: courseRules[0].additionalType,
              targetType: courseRules[0].targetType
            };
          }
          this._loadingService.hideLoading();
        } else { // programCourseObject is null - show error modal
          const title = this._translateService.instant('StudyPrograms.CopyRulesError');
          const message = this._translateService.instant('StudyPrograms.CopyRulesErrorMessage');
          this._modalService.showDialog(title, message, DIALOG_BUTTONS.Ok).then(dialogResult => {
            return;
          });
        }
      this.courses.selectNone();
    }
  }

  async paste() {
    // Show confirmation modal first
    try {
      const title = this._translateService.instant('StudyPrograms.PasteRules');
      const message = this._translateService.instant('StudyPrograms.PasteRulesAdditionalMessage');
      this._modalService.showDialog(title, message, DIALOG_BUTTONS.YesNo).then(async dialogResult => {
        if (dialogResult === 'no') {
          this.courses.selectNone();
          return;
        }
        this._loadingService.showLoading();

        await Promise.all(this.courses.selected.map(async course => {
          const courseID = course.courseId;

          // Get programCourse ID for each of the selected courses
          return this._context.model('StudyProgramCourses')
            .where('studyProgram').equal(this.studyProgram)
            .and('course').equal(courseID)
            .getItem().then(async (programCourseObject) => {
              if (programCourseObject) {
                if (this.copyRulesData.source !== programCourseObject.id) {
                  this.copyRulesData.destination = programCourseObject.id;
                  await this._context.model(`Rules/copyRules`).save(this.copyRulesData);
                }
              } else { // programCourseObject is null - show error modal
                this._modalService.showDialog(this._translateService.instant('StudyPrograms.PasteRulesError'),
                  this._translateService.instant('StudyPrograms.PasteRulesErrorMessage'), DIALOG_BUTTONS.Ok).then(result => {
                  return;
                });
              }
            });
        }));
        this.courses.selectNone();
        this._loadingService.hideLoading();
      });
    } catch (err) {
      this.courses.selectNone();
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }


}
