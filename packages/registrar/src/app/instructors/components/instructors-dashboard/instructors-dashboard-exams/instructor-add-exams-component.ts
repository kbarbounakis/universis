import {Component, Input} from '@angular/core';
// tslint:disable-next-line:max-line-length
import {
  AdvancedTableModalBaseComponent,
  AdvancedTableModalBaseTemplate
} from '@universis/ngx-tables';
import {ActivatedRoute, Router} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import {ErrorService, ToastService} from '@universis/common';
import {TranslateService} from '@ngx-translate/core';
import {AdvancedFilterValueProvider} from '@universis/ngx-tables';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-instructor-add-exams',
  template: AdvancedTableModalBaseTemplate
})
export class InstructorAddExamsComponent extends AdvancedTableModalBaseComponent {

  @Input() instructor: any;

  constructor(_router: Router, _activatedRoute: ActivatedRoute,
              _context: AngularDataContext,
              private _errorService: ErrorService,
              private _toastService: ToastService,
              private _translateService: TranslateService,
              protected advancedFilterValueProvider: AdvancedFilterValueProvider,
              protected datePipe: DatePipe) {
    super(_router, _activatedRoute, _context, advancedFilterValueProvider, datePipe);
    this.modalTitle = 'Instructors.AddExam';
  }

  hasInputs(): Array<string> {
    return [ 'instructor' ];
  }

  ok(): Promise<any> {
    const selected = this.advancedTable.selected;
    let items = [];
    if (selected && selected.length > 0) {
      items = selected.map( courseExam => {
        return {
          instructor: this.instructor,
          courseExam: courseExam
        };
      });
      return this._context.model('courseExamInstructors')
        .save(items)
        .then( result => {
          // add toast message
          this._toastService.show(
            this._translateService.instant('Instructors.AddExamsMessage.title'),
            this._translateService.instant((items.length === 1 ?
              'Instructors.AddExamsMessage.one' : 'Instructors.AddExamsMessage.many')
              , { value: items.length })
          );
          return this.close({
            fragment: 'reload',
            skipLocationChange: true
          });
        }).catch( err => {
          this._errorService.showError(err, {
            continueLink: '.'
          });
        });
    }
    return this.close();
  }
}
