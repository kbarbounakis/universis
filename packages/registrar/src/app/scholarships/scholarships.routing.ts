import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ScholarshipsHomeComponent} from './components/scholarships-home/scholarships-home.component';
import {ScholarshipsTableComponent} from './components/scholarships-table/scholarships-table.component';
import {ScholarshipsPreviewComponent} from './components/scholarships-preview/scholarships-preview.component';
import {ScholarshipsRootComponent} from './components/scholarships-root/scholarships-root.component';
import {ScholarshipsPreviewGeneralComponent} from './components/scholarships-preview/scholarships-preview-general/scholarships-preview-general.component';
import {ScholarshipsPreviewStudentsComponent} from './components/scholarships-preview/scholarships-preview-students/scholarships-preview-students.component';
import {AdvancedFormRouterComponent} from '@universis/forms';
import {
  ScholarshipsTableConfigurationResolver,
  ScholarshipsTableSearchResolver
} from './components/scholarships-table/scholarships-table-config.resolver';
import {AdvancedFormItemResolver, AdvancedFormModalData} from '@universis/forms';
import {ScholarshipsAddStudentComponent} from './components/scholarships-preview/scholarships-preview-students/scholarships-add-student.component';
import {ItemRulesModalComponent, RuleFormModalData} from '../rules';
import {
  ActiveDepartmentIDResolver,
  ActiveDepartmentResolver, CurrentAcademicPeriodResolver,
  CurrentAcademicYearResolver
} from '../registrar-shared/services/activeDepartmentService.service';
import {ScholarshipPreviewResultsComponent} from './components/scholarships-preview/scholarship-preview-results/scholarship-preview-results.component';
import {ScholarshipResultsTableSearchResolver} from './components/scholarships-preview/scholarship-preview-results/scholarship-preview-results-config.resolver';
import { SelectReportComponent } from '../reports-shared/components/select-report/select-report.component';


const routes: Routes = [
    {
        path: '',
        component: ScholarshipsHomeComponent,
        data: {
            title: 'Scholarships'
        },
        children: [
            {
                path: '',
                pathMatch: 'full',
                redirectTo: 'list/open'
            },
            {
              path: 'list',
              pathMatch: 'full',
              redirectTo: 'list/open'
            },
            {
                path: 'list/:list',
                component: ScholarshipsTableComponent,
                data: {
                    title: 'Scholarships'
                },
                resolve: {
                  tableConfiguration: ScholarshipsTableConfigurationResolver,
                  searchConfiguration: ScholarshipsTableSearchResolver
                },
                children: [
                  {
                    path: 'item/:id/rules',
                    pathMatch: 'full',
                    component: ItemRulesModalComponent,
                    outlet: 'modal',
                    data: <RuleFormModalData> {
                      model: 'Scholarships',
                      closeOnSubmit: true,
                      navigationProperty: 'ScholarshipRules'
                    },
                    resolve: {
                      department: ActiveDepartmentIDResolver,
                      data: AdvancedFormItemResolver
                    }
                  },
                ]
            }

        ]
    },
    {
      path: 'new',
      component: ScholarshipsRootComponent,
      children: [
        {
          path: '',
          pathMatch: 'full',
          redirectTo: 'new'
        },
        {
          path: 'new',
          component: AdvancedFormRouterComponent
        }
      ],
      resolve: {
        department: ActiveDepartmentResolver
      }
    },
    {
        path: ':id',
        component: ScholarshipsRootComponent,
        data: {
            title: 'Scholarships Home'
        },
        children: [
            {
                path: '',
                pathMatch: 'full',
                redirectTo: 'preview'
            },
            {
                path: 'preview',
                component: ScholarshipsPreviewComponent,
                data: {
                    title: 'Scholarships Preview'
                },
                children: [
                    {
                        path: '',
                        redirectTo: 'general'
                    },
                  {
                    path: 'general',
                    component: ScholarshipsPreviewGeneralComponent,
                    data: {
                      title: 'Scholarships Preview General'
                    }, children: [
                      {
                        path: 'rules',
                        pathMatch: 'full',
                        component: ItemRulesModalComponent,
                        outlet: 'modal',
                        data: <RuleFormModalData>{
                          model: 'Scholarships',
                          closeOnSubmit: true,
                          navigationProperty: 'ScholarshipRules'
                        },
                        resolve: {
                          department: ActiveDepartmentIDResolver,
                          data: AdvancedFormItemResolver
                        }
                      },
                      {
                        path: 'print',
                        pathMatch: 'full',
                        component: SelectReportComponent,
                        outlet: 'modal',
                        resolve: {
                          item: AdvancedFormItemResolver
                        }
                      }
                    ]
                  },
                    {
                        path: 'students',
                        component: ScholarshipsPreviewStudentsComponent,
                        data: {
                            title: 'Scholarships Preview Students'
                        },
                        children: [
                          {
                            path: 'add',
                            pathMatch: 'full',
                            component: ScholarshipsAddStudentComponent,
                            outlet: 'modal',
                            data: <AdvancedFormModalData> {
                              title: 'Scholarships.AddStudent'
                            },
                            resolve: {
                              scholarship: AdvancedFormItemResolver
                            }
                          }
                        ]
                    },
                  {
                    path: 'results',
                    component: ScholarshipPreviewResultsComponent,
                    data: {
                      title: 'Scholarships Preview Results'
                    },
                    resolve: {
                      searchConfiguration: ScholarshipResultsTableSearchResolver
                    }
                  }
                  ]

            },
            {
              path: ':action',
              component: AdvancedFormRouterComponent
            }

        ]
  }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    declarations: []
})
export class ScholarshipsRoutingModule {
}
