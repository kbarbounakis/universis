import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { DIALOG_BUTTONS, ErrorService, LoadingService, ModalService, ToastService } from '@universis/common';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-enrollment-event-viewers',
  templateUrl: './event-overview-viewers.component.html'
})
export class EnrollmentEventViewersComponent implements OnInit, OnDestroy {

  @Input() public enrollmentEvent: any;
  private subscription: Subscription;
  public model: any = {};
  public lastError: any;
  public currentViewers: any;

  constructor(
    private _context: AngularDataContext,
    private _loadingService: LoadingService,
    private _errorService: ErrorService,
    private _modalService: ModalService,
    private _translateService: TranslateService,
    private _toastService: ToastService
  ) { }

  public requestSummary;
  public total;

  async ngOnInit() {
    this._loadingService.showLoading();

    // Get the viewers for the current enrollment event
    // We expand the viewers to include the departments
    // This is so we can display the department names in the table
    this.currentViewers = await this._context.model('StudyProgramEnrollmentEvents')
      .where('id').equal(this.enrollmentEvent.id)
      .expand('viewers($expand=departments($expand=locales))').getItem();

    // prepare departments names to be displayed in the viewers table
    // by joining the first 5 department names with a comma and adding an ellipsis if there are more than 5 departments
    // otherwise display a hyphen
    this.currentViewers.viewers.forEach(viewer => {
      viewer.departments.name = viewer.departments && Array.isArray(viewer.departments) && viewer.departments.length > 0
        ? (viewer.departments.slice(0, 5).map(x => x.name).join(', ') + (viewer.departments.length > 5 ? '...' : ''))
        : '-';
    });
    this._loadingService.hideLoading();
  }

  /**
   * Remove a viewer from the current enrollment event
   * @param viewer The viewer object to be removed
   */
  remove(viewer: any) {
    const item: any = {
      event: this.enrollmentEvent.id,
      viewer: viewer.id
    };

    // Show a warning dialog to confirm the user wants to remove the viewer
    return this._modalService.showWarningDialog(
      // Title of the dialog
      this._translateService.instant('Candidates.Viewers.RemoveViewerTitle'),
      // Message to be displayed in the dialog
      // The message includes the viewer name and department name
      this._translateService.instant('Candidates.Viewers.RemoveViewerMessage', { viewerName: viewer.alternateName, department: viewer.departments.name }),
      // Buttons to be displayed in the dialog
      DIALOG_BUTTONS.OkCancel
    ).then(result => {
      if (result === 'ok') {
        // Remove the viewer from the enrollment event
        this._context.model('StudyProgramEnrollmentEventViewers').remove(item).then(() => {
          // Show a toast message to confirm the viewer has been removed
          this._toastService.show(
            // Title of the toast message
            this._translateService.instant('Candidates.Viewers.RemoveViewersMessage.title'),
            // Message to be displayed in the toast message
            this._translateService.instant('Candidates.Viewers.RemoveViewersMessage.one')
          );
          // Reload the component to reflect the changes
          this.ngOnInit();
        }).catch(err => {
          // Catch any errors that occur and show a message to the user
          this._errorService.showError(err, {
            continueLink: '.'
          });
        });
      }
    });
  }

  /**
   * Called when the component is destroyed.
   * Unsubscribes from the subscription to the router params.
   */
  ngOnDestroy() {
    // Unsubscribe from the subscription to the router params
    // This is necessary to prevent memory leaks
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
