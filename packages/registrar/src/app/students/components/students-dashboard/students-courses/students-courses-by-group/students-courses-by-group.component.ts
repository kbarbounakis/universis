import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { ActivatedRoute } from '@angular/router';
import { AdvancedAggregatorFilters, AdvancedSearchFormComponent } from '@universis/ngx-tables';
import * as SEARCH_CONFIG from './student-courses-table.search.list.json';
import { ConfigurationService, DIALOG_BUTTONS, ErrorService, GradeScale, LoadingService, LocalizedDatePipe, ModalService,
   TemplatePipe, ToastService } from '@universis/common';
import { StudentsCoursesExemptionComponent } from '../students-courses-exemption/students-courses-exemption.component';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { AdvancedFilterValueProvider } from '@universis/ngx-tables';
import { EditCoursesComponent } from '../../../../../study-programs/components/preview/edit-courses/edit-courses.component';
import { AdvancedAggregatorConfig } from '@universis/ngx-tables';
import { GradesService } from './../../../../services/grades-service/grades-service';
import * as STUDENTS_COURSES_GROUP from './students-courses-by-group.config.json';
import { TranslateService } from '@ngx-translate/core';
import { take } from 'rxjs/operators';
import { scaleUsed } from '../students-courses-percentages-edit/students-courses-percentages-edit.component';
import { StudentsCourseEditComponent } from '../students-course-edit/students-course-edit.component';
import { isEqual, cloneDeep } from 'lodash';

@Component({
  selector: 'app-students-courses-by-group',
  templateUrl: './students-courses-by-group.component.html',
  styleUrls: ['./students-courses-by-group.component.scss']
})
export class StudentsCoursesByGroupComponent implements OnInit, OnDestroy {
  public model: any;
  public data: any;
  public groups = [];
  public studentId;
  private subscription: Subscription;
  public student: any;
  public advancedAggregatorConfiguration = STUDENTS_COURSES_GROUP as any as AdvancedAggregatorConfig;
  public studentCoursesConfig = STUDENTS_COURSES_GROUP as any;
  private selectedGroups = new BehaviorSubject({});
  public filters = new BehaviorSubject({ filters: [], text: undefined });
  public selectedGroups$: Observable<any>;
  public filters$: Observable<any>;

  @ViewChild('search') search: AdvancedSearchFormComponent;
  collapsed = true;

  // tslint:disable-next-line:max-line-length
  public searchExpression = '(student eq ${student} and (indexof(course/name, \'${text}\') ge 0 or indexof(course/displayCode, \'${text}\') ge 0 or ' +
    // tslint:disable-next-line:max-line-length
    'indexof(course/courseArea/name, \'${text}\') ge 0 or indexof(course/courseCategory/name, \'${text}\') ge 0 or indexof(courseType/name, \'${text}\') ge 0))';

  public allStudentCourses: any;

  public defaultGradeScale: any;

  public gradeScale: GradeScale;
  public aggregatorList = {
    'total-grade-included-courses': (items: any[]) => {
      return items
        .filter((item) => item.isPassed || item.registrationType === 1)
        .filter((item) => item.calculateUnits)
        .length;
    },
    'simple-grade-average': (items) => this.calculateSimpleGradeAverage(items),
    'formatted-grade-average': (items) => this.calculateFormattedGradeAverage(items),
    'passed-ects': (items: any[]) => {
      return items
        .filter((item) => item.isPassed)
        .filter((item) => item.calculateUnits)
        .reduce((total, item) => total + item.ects, 0)
        .toFixed(2);
    }
  };

  public postProcessors;

  public group1: any;
  public group2: any;
  public aggregatorListData = {};

  constructor (
    private _activatedRoute: ActivatedRoute,
    private _context: AngularDataContext,
    private _template: TemplatePipe,
    private _modalService: ModalService,
    private _loadingService: LoadingService,
    private _errorService: ErrorService,
    private _advancedFilter: AdvancedFilterValueProvider,
    private _configService: ConfigurationService,
    private _translateService: TranslateService,
    private _gradesService: GradesService,
    private _toastService: ToastService,
    private _localizedDatePipe: LocalizedDatePipe) { }

  async ngOnInit() {
    this.filters = new BehaviorSubject({ filters: [], text: undefined });
    const defaultGroup = this.advancedAggregatorConfiguration.groupProperties.find(
      (item) => item.default
    ) || this.advancedAggregatorConfiguration.groupProperties[0];
    this.group1 = defaultGroup;
    this.selectedGroups = new BehaviorSubject([defaultGroup]);
    this.selectedGroups$ = this.selectedGroups.asObservable();
    this.filters$ = this.filters.asObservable();
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      try {
        this._loadingService.showLoading();
        this.student = await this._context.model('Students')
          .where('id')
          .equal(params.id)
          .expand('studentStatus, studyProgram($expand=gradeScale)')
          .getItem();

        this.aggregatorListData = {
          student: this.student.id
        };

        this.defaultGradeScale = this.student && this.student.studyProgram && this.student.studyProgram.gradeScale;

        if (this.defaultGradeScale.scaleType !== 0) {
          this.defaultGradeScale = await this._context.model('GradeScales')
            .where('id').equal(this.defaultGradeScale.id)
            .expand('values')
            .getItem();
        }

        this.gradeScale = new GradeScale(this._configService.currentLocale, this.defaultGradeScale);
        this.model = await this._context.model('StudentCourses')
          .where('student').equal(params.id)
          // tslint:disable-next-line:max-line-length
          .expand('programGroup, course($expand=instructor,gradeScale),gradeExam($expand=instructors($expand=instructor)), studyProgramSpecialty')
          .take(-1)
          .getItems();

        this.allStudentCourses = this.model;
        this.postProcessors = {
          'combine-complex': (items) => {
            return this.parseCourseParts(this.allStudentCourses, items);
          },
          'combine-exam-period-data': (items) => {
            // tslint:disable-next-line:no-shadowed-variable
            return ((items) => {
              items.forEach(x => {
                // tslint:disable-next-line:max-line-length
                if (x.gradeExam && x.gradeExam.examPeriod && x.gradeExam.examPeriod.alternateName && x.gradeExam.year && x.gradeExam.year.alternateName) {
                  x.gradeExam.year.alternateName = x.gradeExam.examPeriod.alternateName + ' ' + x.gradeExam.year.alternateName;
                } else {
                  // gradeExam does not exist either because course has an exception or it is complex
                  if (x.gradeYear && x.gradeYear.alternateName && !x.gradeExam) {
                    // assign gradeYear to gradeExam
                    x.gradeExam = {
                      year: x.gradeYear
                    };
                    if (x.examPeriod && x.examPeriod.alternateName) {
                      x.gradeExam.year.alternateName = x.examPeriod.alternateName + ' ' + x.gradeExam.year.alternateName;
                    } else {
                      if (x.gradePeriod && x.gradePeriod.alternateName) {
                        x.gradeExam.year.alternateName = x.gradePeriod.name + ' ' + x.gradeExam.year.alternateName;
                      }
                    }
                  }
                }
              });
              return items;
            })(items);
          },
          'format-exam-date': (items) => {
            return ((items) => {
              items.forEach(x => {
                if (x.gradeExam && x.gradeExam.examDate) {
                  x.gradeExam.examDate = this._localizedDatePipe.transform(x.gradeExam.examDate,'shortDate');
                }
              });
              return items;
            })(items);
          }

        };
        this._loadingService.hideLoading();
        this.search.form = SEARCH_CONFIG;
        Object.assign(this.search.form, { student: params.id });
        this.search.ngOnInit();
      } catch (err) {
        this._loadingService.hideLoading();
        this._errorService.showError(err, {
          continueLink: '.'
        });
      }
    }, (err) => {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  parseCourseParts(allItems: any[], items: any[]) {
    // get parent courses for orphaned children after search
    const parents = allItems.filter(el => {
      return !items.map(x => x.course.id).includes(el.course.id);
    }).filter(el => {
      return items.map(x => x.parentCourse).includes(el.course.id);
    });

    let data = [];
    try {
      // remove course parts and add parents
      data = items.filter(studentCourse => {
        return studentCourse.courseStructureType && studentCourse.courseStructureType.id !== 8;
      }).concat(parents);
    } catch (err) {
      console.error('err: ', err);
    }

    // add courseParts as courses to each parent course
    data.forEach(studentCourse => {
      // get course parts
      if (studentCourse.courseStructureType && studentCourse.courseStructureType.id === 4) {
        studentCourse.subitems = items.filter(course => {
          return course.parentCourse === studentCourse.course.id;
        }).sort((a, b) => {
          return a.course.displayCode < b.course.displayCode ? -1 : 1;
        });

        if (studentCourse.subitems.length) {
          const scale = scaleUsed(studentCourse.subitems);
          studentCourse.subitems.forEach(x => {
            x.courseTitle += ` (${Number(x.coursePercent || 0) * (scale === 1 ? 100 : 1)}%)`;
            // combine gradeYear with gradeExam for course parts
            if (x.gradeExam && x.gradeExam.examPeriod && x.gradeExam.examPeriod.alternateName
              && x.gradeExam.year && x.gradeExam.year.alternateName) {
              x.gradeExam.year.alternateName = x.gradeExam.examPeriod.alternateName + ' ' + x.gradeExam.year.alternateName;
            } else {
              // gradeExam does not exist because course has an exception
              if (x.gradeYear && x.gradeYear.alternateName && !x.gradeExam) {
                // assign gradeYear to gradeExam
                x.gradeExam = {
                  year: x.gradeYear
                };
                if (x.gradePeriod && x.gradePeriod.alternateName) {
                  x.gradeExam.year.alternateName = x.gradePeriod.name + ' ' + x.gradeExam.year.alternateName;
                }
              }
            }
            if (x.gradeExam && x.gradeExam.examDate) {
              x.gradeExam.examDate = this._localizedDatePipe.transform(x.gradeExam.examDate, 'shortDate');
            }
          });
        }
      }
    });

    return data;
  }

  async onSearchKeyDown(event: any) {
    const searchText = (<HTMLInputElement>event.target);
    if (searchText && event.keyCode === 13) {
      this.subscription = this._activatedRoute.params.subscribe(async (params) => {
        this.studentId = params.id;
        this.filters.next({
          filters: [],
          text: searchText.value
        });
        // this.parseCourseParts();
      });
    }
  }

  async advancedSearch(event?: any) {
    if (Array.isArray(this.search.form.criteria)) {
      const expressions = [];
      const filter = this.search.filter;
      const values = this.search.formComponent.formio.data;
      this.subscription = this._activatedRoute.params.subscribe(async (params) => {
        this.studentId = params.id;

        this.search.form.criteria.forEach((x) => {

          if (Object.prototype.hasOwnProperty.call(filter, x.name)) {
            // const nameFilter = this.convertToString(filter[x.name]);
            if (filter[x.name] !== 'undefined') {
              expressions.push(this._template.transform(x.filter, Object.assign({
                value: filter[x.name]
              }, values)));
            }
          }
        });
        // expressions.push('(student eq ' +  this.studentId  + ')');

        this.filters.next({ filters: expressions, text: undefined });
        // this.parseCourseParts();
      });
    }
  }

  fetchProgramGroup() {
    throw new Error('Method not implemented.');
  }

  async editCourse(entry: any) {

    const course = await this._context.model('StudentCourses')
      .where('student').equal(this.student.id)
      .where('id').equal(entry.id)
      // tslint:disable-next-line:max-line-length
      .expand('programGroup, studyProgramSpecialty, locales, course($expand=instructor, gradeScale), gradeExam($expand=instructors($expand=instructor))')
      .take(1)
      .getItem();
    // prepare locales for edit
    if (course &&
        Array.isArray(course.locales) &&
        this._configService.settings &&
        this._configService.settings.i18n &&
        Array.isArray(this._configService.settings.i18n.locales) &&
        this._configService.settings.i18n.defaultLocale) {
          // exclude default application locale, as defined
          course.locales = course.locales.filter((locale: {inLanguage: string}) => {
            return locale.inLanguage !== this._configService.settings.i18n.defaultLocale;
          });
          // for each of the non-default locales
          this._configService.settings.i18n.locales
            .filter((locale: string) => locale !== this._configService.settings.i18n.defaultLocale)
            .forEach((locale: string) => {
              // try to find if item exists in student course locales
              const localeExists = course.locales.find((courseLocale: {inLanguage: string}) => {
                return courseLocale.inLanguage === locale;
              });
              // if it does not
              if (localeExists == null) {
                // push it
                course.locales.push({
                  inLanguage: locale
                });
              }
            });
    }
    // hold previous locales state to prevent unnecessary update
    const previousLocaleState = cloneDeep(course.locales);
    // if the course is complex, configure the additional modal's data
    const courseParts = course.courseStructureType.id === 4 && this.model.filter(part => part.parentCourse === course.course.id);
    const extraData: any = {};
    if (courseParts) {
      // Changes applied to course's parts must be discarded when user presses cancel.
      // Pass a deep copy copy of the courseParts to achieve this.
      extraData.complexCourses = [{ ...course, parts: JSON.parse(JSON.stringify(courseParts))}];
      extraData.reloadParent = () => {
        // trigger the update of the list
        this.filters.next(this.filters.getValue());

        // update the model
        this._context.model('StudentCourses')
          .where('student').equal(this.student.id)
          // tslint:disable-next-line: max-line-length
          .expand('programGroup, course($expand=instructor,gradeScale),gradeExam($expand=instructors($expand=instructor)), studyProgramSpecialty')
          .take(-1)
          .getItems()
          .then(res => this.model = res);
      };
    }

    const formName = ['declared', 'graduated'].includes((this.student && this.student.studentStatus &&
      this.student.studentStatus.alternateName)) ? 'StudentCourses/course-attributes/editGraduated'
      : 'StudentCourses/course-attributes/edit';
    this._loadingService.showLoading();
    this._modalService.openModalComponent(courseParts ? StudentsCourseEditComponent : EditCoursesComponent, {
      class: 'modal-xl',
      keyboard: true,
      ignoreBackdropClick: true,
      initialState: {
        ...extraData,
        items: [course],
        modalTitle: 'Students.EditCourse',
        formEditName: formName,
        toastHeader: 'Students.EditStudentCourse',
        courseProperties: {
          courseTitle: course.courseTitle,
          notes: course.notes,
          calculateGrade: !!course.calculateGrade,
          calculateUnits: !!course.calculateUnits,
          specialty: course.studyProgramSpecialty,
          programGroup: course.programGroup ? course.programGroup : {},
          student: this.student,
          registrationType: course.registrationType !== 0,
          formattedGradeNum: !course.course.gradeScale.scaleType ? course.formattedGrade : null,
          formattedGradeVal: course.course.gradeScale.scaleType ? course.formattedGrade : null,
          gradeScale: course.course.gradeScale,
          gradeYear: course.gradeYear,
          gradePeriod: course.gradePeriod,
          locales: course.locales
        },
        deleteCourseButton: !!(this.student && this.student.studentStatus &&
          this.student.studentStatus.alternateName === 'active'),
        execute: (() => {
          return new Observable((observer) => {
            this._loadingService.showLoading();
            // get edit courses component
            const component = <EditCoursesComponent>this._modalService.modalRef.content;
            // fix data structure
            const submissionData = component.items.map(async (x) => {
              if (x.delete) {
                return x;
              }
              if (x.gradeScale.scaleType) {
                x.formattedGrade = x.formattedGradeVal;
              } else {
                x.formattedGrade = x.formattedGradeNum;
              }

              let currentGradeScale;
              if (x.course && x.course.gradeScale && x.course.gradeScale.id && x.course.gradeScale.id !== this.defaultGradeScale.id) {
                const item = await this._context.model('GradeScales')
                  .where('id').equal(x.course.gradeScale.id)
                  .expand('values')
                  .getItem();

                currentGradeScale = new GradeScale(this._configService.currentLocale, item);
              } else {
                currentGradeScale = this.gradeScale;
              }

              return {
                calculateGrade: x.calculateGrade ? 1 : 0,
                calculateUnits: x.calculateUnits ? 1 : 0,
                coefficient: x.coefficient,
                course: x.course.id,
                courseTitle: x.courseTitle,
                courseType: x.courseType,
                ects: x.ects,
                notes: x.notes,
                semester: x.semester,
                student: x.student,
                units: x.units,
                specialty: x.specialty.specialty,
                programGroup: x.programGroup && x.programGroup.id ? x.programGroup.id : null,
                gradeYear: x.gradeYear && x.gradeYear.id ? x.gradeYear : null,
                gradePeriod: x.gradePeriod && x.gradePeriod.id ? x.gradePeriod : null,
                grade: x.formattedGrade ? currentGradeScale.convert(x.formattedGrade) : null,
                registrationType: x.registrationType ? 1 : 0,
                // note: do not filter by courseTitle (maybe the user wants to "delete" the title)
                locales: x.locales
              };
            });
            // and submit
            Promise.all(submissionData).then(res => {

              if (res[0] && res[0].delete) { // Delete current student course
                Object.assign(res[0], { $state: 4 });
              }
              if (res && res[0] && isEqual(previousLocaleState, res[0].locales)) {
                delete res[0].locales;
              }
              // Save edited form
              this._context.model('StudentCourses').save(res).then(async () => {
                this.filters.next(this.filters.getValue());
                this._loadingService.hideLoading();
                observer.next();
              }).catch(async (err) => {
                this._loadingService.hideLoading();
                observer.error(err);
                if (res[0].delete) {
                  await this._modalService.showErrorDialog(this._translateService.instant('Students.RemoveCourseTitle'),
                  err.error.code, DIALOG_BUTTONS.Ok);
                }
              });
            }).catch(err => {
              this._loadingService.hideLoading();
              observer.error(err);
              console.log(err);
            });
          });
        })()
      }
    });
    this._loadingService.hideLoading();
  }

  async editCourses() {
    const student = (await this._context.model('Students')
      .where('id').equal(this._activatedRoute.snapshot.params.id).select('studyProgram, specialtyId').getItem());
    this._advancedFilter.values = {
      ...this._advancedFilter.values,
      'student': this._activatedRoute.snapshot.params.id,
      'program': student.studyProgram,
      'specializationIndex': student.specialtyId
    };
    this._modalService.openModalComponent(StudentsCoursesExemptionComponent, {
      class: 'modal-xl',
      keyboard: true,
      ignoreBackdropClick: false,
      initialState: {
        studyProgram: student.studyProgram,
        student: this._activatedRoute.snapshot.params.id,
        modalTitle: 'StudyPrograms.EditCourses',
        execute: this.executeAction()
      }
    });
  }

  executeAction() {
    return new Observable((observer) => {
      this._loadingService.showLoading();
      // get add courses component
      const component = <StudentsCoursesExemptionComponent>this._modalService.modalRef.content;
      // and submit
      this._context.model('StudentCourses').save(Array.from(component.items)).then(async () => {
        this.filters.next(this.filters.getValue());
        this._loadingService.hideLoading();
        delete this._advancedFilter.values['student'];
        delete this._advancedFilter.values['program'];
        delete this._advancedFilter.values['specializationIndex'];
        observer.next();
      }).catch((err) => {
        this._loadingService.hideLoading();
        observer.error(err);
      });
    });
  }


  async onActionEvent(event) {
    if (event.type === 'editCourse') {
      await this.editCourse(event.data);
    }
  }

  calculateSimpleGradeAverage(items: any[]) {
    try {
      const formattedItems = items.map(x => {
        return {
          ...x,
          courseStructureType: x['courseStructureType/id']
        };
      });
      const average = this._gradesService.getGradesSimpleAverage(formattedItems).average;
      return this.gradeScale.format(average);
    } catch (err) {
      console.error(err);
      return '-';
    }
  }

  calculateFormattedGradeAverage(items: any[]) {
    try {
      const formattedItems = items.map(x => {
        return {
          ...x,
          courseStructureType: x['courseStructureType/id']
        };
      });
      const average = this._gradesService.getGradesWeightedAverage(formattedItems).average;
      return this.gradeScale.format(average);
    } catch (err) {
      console.error(err);
      return '-';
    }

  }

  setPrimaryGroup(group) {
    this.group1 = group;
    this.group2 = undefined;
    if (group) {
      this.selectedGroups.next([this.group1]);
    } else {
      this.selectedGroups.next([]);
    }
  }

  setSecondaryGroup(group) {
    if (this.group1.property !== group.property) {
      this.group2 = group;
      this.selectedGroups.next([this.group1, this.group2]);
    }
  }


  async exportCourses() {
    const headers = new Headers();
    // get service headers.
    const serviceHeaders = this._context.getService().getHeaders();
    Object.keys(serviceHeaders).forEach((key) => {
      if (serviceHeaders.hasOwnProperty(key)) {
        headers.set(key, serviceHeaders[key]);
      }
    });
    // set accept header.
    headers.set('Accept', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

    // get properties from columns
    const properties = JSON.parse(JSON.stringify(this.studentCoursesConfig.columns.filter(f => !f.hidden)));
    if (!(properties && properties.length)) {
      return;
    }

    // get other properties from groupProperties
    this.studentCoursesConfig.groupProperties.forEach(x => {
      const valueCopy = Object.assign({}, x);
      if (properties.findIndex(y => valueCopy.name === y.name || valueCopy.label === y.title) === -1) {
        valueCopy.title = valueCopy.label;
        valueCopy.default = false;
        properties.push(valueCopy);
      }
    });

    const selectProperties = properties.map(x => {
      if (x.title) {
        return `${x.name} as "${this._translateService.instant(x.title)}"`;
      }
      return x.name;
    }).join(',');
    // tslint:disable-next-line:max-line-length
    let fileURL = this._context.getService().resolve(`Students/${this.student.id}/courses?$top=-1&$orderby=semester&$select=${selectProperties}`);

    this.filters$.pipe(take(1)).subscribe(async (filters: AdvancedAggregatorFilters) => {
      try {
        if (filters && ((filters.filters && filters.filters.length) || filters.text)) {
          const filter = await this.prepareFilter(this.aggregatorListData,
            filters.filters,
            this.searchExpression,
            filters.text
          );
          fileURL += filter;
        }

        fetch(fileURL, {
          headers: headers,
          credentials: 'include'
        }).then((response) => {
          return response.blob();
        }).then(blob => {
          const objectUrl = window.URL.createObjectURL(blob);
          const a = document.createElement('a');
          document.body.appendChild(a);
          a.setAttribute('style', 'display: none');
          a.href = objectUrl;
          const name = 'StudentCourses-' + this.student.studentIdentifier;
          const extension = 'xlsx';
          const downloadName = `StudentCourses-${this.student.studentIdentifier}.${extension}`;
          a.download = downloadName;
          a.click();
          window.URL.revokeObjectURL(objectUrl);
          a.remove();
        });
      } catch (err) {
        console.error(err);
      }
    });
  }

  async prepareFilter(fixedParams: object, filters: string[], searchTemplate?: string, searchValue?: string): Promise<any> {
    if (!fixedParams || typeof fixedParams !== 'object') {
      fixedParams = {};
    }

    let filterExpressions = [];

    if (fixedParams) {
      for (const param of Object.keys(fixedParams)) {
        filterExpressions.push(`${param} eq ${fixedParams[param]}`);
      }
    }

    if (filters && Array.isArray(filters)) {
      filterExpressions = [
        ...filterExpressions,
        ...filters
      ];
    }

    if (searchTemplate && searchValue) {
      let evaluatedSearch;
      if (/^"(.*?)"$/.test(searchValue)) { // validate search text in double quotes
        evaluatedSearch = this._template.transform(searchTemplate, {
          ...fixedParams,
          text: searchValue.replace(/^"|"$/g, '')
        });
      } else {
        // try to split words
        const words = searchValue.split(' ');
        // join words to a single filter
        evaluatedSearch = words.map(word => {
          return this._template.transform(searchTemplate, {
            ...fixedParams,
            text: word
          });
        }).join(' and ');

      }
      filterExpressions.push(evaluatedSearch);
    }

    return `&$filter=${filterExpressions.join(' and ')}`;
  }

}
