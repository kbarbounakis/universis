import { Component, Input, OnInit, } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { TranslateService } from '@ngx-translate/core';
import { ModalService, ToastService, LoadingService } from '@universis/common';
import { RouterModalOkCancel } from '@universis/common/routing';

export function scaleUsed(courseParts) {
  for (const part of courseParts) {
    if (Number(part.coursePercent || 0) > 1) { return 100; }
  }
  return 1;
}

@Component({
  selector: 'app-students-courses-percentages-edit',
  templateUrl: './students-courses-percentages-edit.component.html'
})
export class StudentsCoursesPercentagesEditComponent extends RouterModalOkCancel implements OnInit {
  @Input() public complexCourses: any;
  @Input() public reloadParent: (modifiedParts: any) => void;
  public selectedCourse;
  public lastError: any;
  public yesNoOptions = {
    labels: ['Yes', 'No'],
    values: [1, 0]
  };

  constructor(router: Router,
    activatedRoute: ActivatedRoute,
    private _context: AngularDataContext,
    private _translateService: TranslateService,
    private _modalService: ModalService,
    private _loadingService: LoadingService,
    private _toastService: ToastService) {

    // call super constructor.
    super(router, activatedRoute);
    this.modalTitle = this._translateService.instant('Forms.EditPartFactors');
    this.okButtonText = this._translateService.instant('Courses.Submit');
    this.modalClass = 'modal-xl';
  }
  ngOnInit() {
    this.selectedCourse = this.complexCourses[0];
    this.onSelectedCourseChange();
  }

  onSelectedCourseChange() {
    const scale = scaleUsed(this.selectedCourse.parts);
    this.selectedCourse.parts.forEach(part => {
      this.validateUserInput(part);
      if (!part.hasOwnProperty('initialPercent')) { part.initialPercent = Number(part.coursePercent); }
      if (!part.hasOwnProperty('initialCalculated')) { part.initialCalculated = Number(part.calculated); }
      if (!part.hasOwnProperty('initialScale')) { part.initialScale = scale; }
    });
    this.okButtonDisabled = !this.validSubmission();
  }

  onPartChange() {
    this.selectedCourse.parts.forEach(part => {
      // coursePercent should be zero when the coursePart isn't calculated or it's initial value is null
      if (!Number(part.calculated) || !part.coursePercent) {
        part.coursePercent = 0;
        part.error = false;
      }
    });
    this.okButtonDisabled = !this.validSubmission();
  }

  validateUserInput(part) {
    const pattern = /^([1-9]\d*|0)(\.\d{1,3})?$/;
    const result = pattern.exec(part.coursePercent);

    // the input must match the above pattern or be less than or equal to 100
    part.error = !result || (Number(part.coursePercent) > 100);
    this.okButtonDisabled = !this.validSubmission();
  }

  validSubmission() {
    // clear last error.
    this.lastError = null;

    try {
      for (const course of this.complexCourses) {
        if (!this.validPercentagesSum(course)) {
          this.selectedCourse.invalidSubmissionMessage = this._translateService.instant('Students.PercentagesInvalidMessage');
          this.selectedCourse.invalidSubmissionMessage += `"${course.courseTitle}"`;
          return false;
        }
      }

      delete this.selectedCourse.invalidSubmissionMessage;
      return true;
    } catch (error) {
      console.log(error);
      this.selectedCourse.invalidSubmissionMessage = this._translateService.instant('Students.PercentagesValidationError');
    }
  }

  validPercentagesSum(course) {
    const complexCourse = course || this.selectedCourse;
    const scale = scaleUsed(complexCourse.parts);
    const nonZeros = complexCourse.parts.filter(course => Number(course.coursePercent) !== 0).length;
    const sum = complexCourse.parts.reduce((a, b) => a + (Number(b.coursePercent) / scale), 0);

    switch (sum) {
      case 0:
      case 1:
      case nonZeros:
        return true;
      default:
        return false;
    }
  }

  getModifiedParts() {
    const modifiedParts = [];
    for (const course of this.complexCourses) {
      const scale = scaleUsed(course.parts);
      for (const part of course.parts) {
        const percentModified = part.hasOwnProperty('initialPercent') && Number(part.coursePercent) !== part.initialPercent;
        const calculatedModified = part.hasOwnProperty('initialCalculated') && Number(part.calculated) !== part.initialCalculated;
        const scaleModified = part.hasOwnProperty('initialScale') && scale !== part.initialScale;

        if (percentModified || calculatedModified || scaleModified) {
          part.scale = scale;
          modifiedParts.push(part);
        }
      }
    }
    return modifiedParts;
  }

  async ok(): Promise<any> {
    this._loadingService.showLoading();
    // gather parts to be saved.
    const partsToBeSaved = this.getModifiedParts().map(part => {
      const percent = Number(part.coursePercent);
      return {
        id: part.id,
        course: part.course,
        courseTitle: part.courseTitle,
        student: part.student,
        // Use toFixed() to bypass floating point misrepresentation, e.g console.log(99.9 /100) prints 0.9990000000000001
        coursePercent: Number((percent / part.scale).toFixed(3)),
        calculated: Number(part.calculated)
      };
    });

    // save modified parts.
    if(partsToBeSaved.length) {
      try {
        const result = await this._context.model(`StudentCourses`).save(partsToBeSaved);
        this.reloadParent(result);

        // inform user.
        this._toastService.show(
          this._translateService.instant('Forms.EditFactorsShort'),
          this._translateService.instant('Forms.EditFactorsSuccess'));
      } catch (err) {
        this._loadingService.hideLoading();
        this.lastError = err;
        console.log(err);
        return false;
      }
    }

    // close modal
    this._loadingService.hideLoading();
    return this.cancel();
  }

  cancel(): Promise<any> {
    if (this._modalService.modalRef) {
      return this._modalService.modalRef.hide();
    }
  }
}
