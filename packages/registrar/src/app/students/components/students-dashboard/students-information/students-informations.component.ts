import {Component, ElementRef, EventEmitter, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {AngularDataContext} from '@themost/angular';
import {DIALOG_BUTTONS, LoadingService, ToastService} from '@universis/common';
import * as STUDENTS_INFORMATIONS_LIST_CONFIG from './students-informations.config.list.json';
import { ActivatedTableService } from '@universis/ngx-tables';
import {
  AdvancedTableComponent,
  AdvancedTableConfiguration,
  AdvancedTableDataResult
} from '@universis/ngx-tables';
import { Subscription } from 'rxjs';
import { ErrorService, ModalService } from '@universis/common';


@Component({
  selector: 'app-students-informations',
  templateUrl: './students-informations.component.html'
})
export class StudentsInformationsComponent implements OnInit, OnDestroy {

  public readonly config: AdvancedTableConfiguration = <AdvancedTableConfiguration>STUDENTS_INFORMATIONS_LIST_CONFIG;

  public recordsTotal: any;
  private dataSubscription: Subscription;
  @ViewChild('informations') informations: AdvancedTableComponent;
  private fragmentSubscription: Subscription;
  @Input() reload: EventEmitter<any> = new EventEmitter<any>();
  @Input() tableConfiguration: any;
  private subscription: Subscription;
  // public model: any;

  constructor(private _element: ElementRef,
              private _activatedRoute: ActivatedRoute,
              private _translate: TranslateService,
              private _loadingService: LoadingService,
              private _context: AngularDataContext,
              private _activatedTable: ActivatedTableService,
              private _errorService: ErrorService,
              private _modalService: ModalService,
              private _translateService: TranslateService,
              private _toastService: ToastService) { }

  async ngOnInit() {

    this._activatedTable.activeTable = this.informations;

    this._loadingService.showLoading();
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.informations.query = this._context.model('StudentInformations')
        .where('student').equal(params.id)
        .expand('infoType')
        .orderByDescending('infoDate')
        .prepare();
      this._loadingService.hideLoading();

      this.informations.config = AdvancedTableConfiguration.cast(STUDENTS_INFORMATIONS_LIST_CONFIG);
      this.informations.fetch();


    // do reload by using hidden fragment e.g. /informations#reload
    this.fragmentSubscription = this._activatedRoute.fragment.subscribe(fragment => {
      if (fragment && fragment === 'reload') {
        this.informations.fetch(true);
      }
    });

    this.dataSubscription = this._activatedRoute.data.subscribe(data => {
      if (data.tableConfiguration) {
        this.informations.config = data.tableConfiguration;
        this.informations.ngOnInit();
      }
     });
    });
  }


  remove() {
    if (this.informations && this.informations.selected && this.informations.selected.length) {
      const items = this.informations.selected;
      return this._modalService.showWarningDialog(
        this._translateService.instant('Students.RemoveInfoTitle'),
        this._translateService.instant('Students.RemoveInfoMessage'),
        DIALOG_BUTTONS.OkCancel).then( result => {
        if (result === 'ok') {
          this._context.model('StudentInformations').remove(items).then( () => {
            this._toastService.show(
              this._translateService.instant('Students.RemoveInfoTitle'),
              this._translateService.instant( 'Settings.OperationCompleted'));
            this.informations.fetch(true);
          }).catch( err => {
            this._errorService.showError(err, {
              continueLink: '.'
            });
          });
        }
      });
    }
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }

    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }
}

