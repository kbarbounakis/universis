import {
  Component,
  OnInit,
  OnDestroy,
  ViewChild
} from '@angular/core';
import {
  AdvancedTableComponent,
  AdvancedTableConfiguration,
  ActivatedTableService,
  AdvancedTableDataResult
} from '@universis/ngx-tables';
import { AngularDataContext } from '@themost/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
import { LoadingService, ErrorService, DiagnosticsService } from '@universis/common';
import * as ACADEMIC_CARD_LIST_CONFIG from './students-academic-card.config.list.json';

@Component({
  selector: 'app-students-academic-card',
  templateUrl: './students-academic-card.component.html',
  styleUrls: ['./students-academic-card.component.scss']
})
export class StudentsAcademicCardComponent implements OnInit, OnDestroy {
  @ViewChild('model') model: AdvancedTableComponent;

  public studentId;
  public readonly config: AdvancedTableConfiguration = <AdvancedTableConfiguration>ACADEMIC_CARD_LIST_CONFIG;
  public recordsTotal: any;
  public hasActiveCard: boolean = false;
  public isServiceActivated: boolean = false;
  public studentHasMail: boolean = true;
  public hasAcademicCardConfiguration: boolean = true;
  public hasFailedCards: boolean = false;
  private tableData: AdvancedTableDataResult;
  private subscription: Subscription;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _translate: TranslateService,
    private _context: AngularDataContext,
    private _activatedTable: ActivatedTableService,
    private _loadingService: LoadingService,
    private _errorService: ErrorService,
    private _diagnosticsService: DiagnosticsService,
    private _router: Router
  ) {}

  async ngOnInit() {
    this._activatedTable.activeTable = this.model;

    this._loadingService.showLoading();
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.studentId = params.id;
      this.model.query = this._context.model('AcademicCards')
        .where('student').equal(this.studentId)
        .prepare();
      this._loadingService.hideLoading();
      this.model.config = AdvancedTableConfiguration.cast(ACADEMIC_CARD_LIST_CONFIG);
      this.model.fetch();

      // Check if EscnService is loaded
      this._diagnosticsService.hasService("EscnService").then((result) => {
        this.isServiceActivated = result ? true : false;
        // Redirect user in case the service is not loaded
        if (!this.isServiceActivated) {
          this._router.navigate(['../']);
        }
      });
    });

    const student = await this._context.model("Students")
      .where("id")
      .equal(this.studentId)
      .expand("person, department")
      .select("id", "person/email", "department/organization")
      .getItem();

    // Check if student has a registered mail address
    this.studentHasMail = student.person_email == null ? false : true;

    const academicCardConfiguration = await this._context.model("AcademicCardConfigurations")
      .where("institute")
      .equal(student.department_organization)
      .getItem();

    // Check if there are academic card configuration for the student's institute
    this.hasAcademicCardConfiguration = academicCardConfiguration == null ? false : true;
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.tableData = data;
    this.hasActiveCard = this.tableData.data.find(card => card.isActive === true) != null ? true : false;
    this.hasFailedCards = this.tableData.data.find(card => card.status === "failed") != null ? true : false;
    this.recordsTotal = data.recordsTotal;
  }

  async addCard() {
    const newEsc = {
      student: this.studentId,
      isActive: true
    }
    await this._context.model('AcademicCards')
      .save(newEsc)
      .then(() => {this.model.fetch(true)})
      .catch(error => {
        this._errorService.showError(error, {
          continueLink: '.'
        })
      });
  }

  async cancelCard() {
    // Retrieve student's active card
    const activeCard = await this._context.model("AcademicCards")
      .where("student")
      .equal(this.studentId)
      .and("isActive")
      .equal("true")
      .select("id", "isActive")
      .getItem();

    if (activeCard) {
      // Set isActive to false, to deactivate the card
      activeCard.isActive = false;
      // Save
      await this._context.model("AcademicCards")
        .save(activeCard)
        .then(() => {this.model.fetch(true)})
        .catch(error => {
          this._errorService.showError(error, {
            continueLink: '.'
          })
        });
    }
  }

  async changeStatusOfFailedCards() {
    // Retrieve student's failed cards
    const failedCards = await this._context.model("AcademicCards")
      .where("student")
      .equal(this.studentId)
      .and("status")
      .equal(5)
      .select("id", "isActive", "status")
      .getItems();

    if (failedCards) {
      for (const card of failedCards) {
        // Change status to 1 (pending) so the process of sending the data to the ESC Router gets restarted
        card.status = 1;
        await this._context.model("AcademicCards")
          .save(card)
          .catch(error => {
            this._errorService.showError(error, {
              continueLink: '.'
            })
          });
      }
    }

    this.model.fetch(true);
  }
}
