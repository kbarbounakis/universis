import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentsRegistrationsComponent } from './students-registrations.component';

describe('StudentsRegistrationsComponent', () => {
  let component: StudentsRegistrationsComponent;
  let fixture: ComponentFixture<StudentsRegistrationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentsRegistrationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentsRegistrationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
