import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentsOverviewThesesComponent } from './students-overview-theses.component';

describe('StudentsOverviewThesesComponent', () => {
  let component: StudentsOverviewThesesComponent;
  let fixture: ComponentFixture<StudentsOverviewThesesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentsOverviewThesesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentsOverviewThesesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
