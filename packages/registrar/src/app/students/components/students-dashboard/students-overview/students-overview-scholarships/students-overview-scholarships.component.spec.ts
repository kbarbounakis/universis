import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentsOverviewScholarshipsComponent } from './students-overview-scholarships.component';

describe('StudentsOverviewScholarshipsComponent', () => {
  let component: StudentsOverviewScholarshipsComponent;
  let fixture: ComponentFixture<StudentsOverviewScholarshipsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentsOverviewScholarshipsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentsOverviewScholarshipsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
