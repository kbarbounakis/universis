import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {Subscription} from 'rxjs';
import {ActivatedRoute} from '@angular/router';
import { AppEventService, DIALOG_BUTTONS, ErrorService, LoadingService, ModalService, ToastService } from '@universis/common';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-students-overview-part-time',
  templateUrl: './students-overview-part-time.component.html',
  styles: [`
    .suspension-row:last-child {
      border-bottom: 0 !important;
    }
    .delete-link {
      cursor: pointer
    }
    .delete-link:hover {
      text-decoration: underline !important;
    }
  `]
})
export class StudentsOverviewPartTimeComponent implements OnInit, OnDestroy  {

  public lastPartTime: any;
  @Input() studentId: number;
  private subscription: Subscription;
  private fragmentSubscription: Subscription;
  private reloadSubscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _modalService: ModalService,
              private _toastService: ToastService,
              private _translateService: TranslateService,
              private _loadingService: LoadingService,
              private _errorService: ErrorService,
              private _appEventService: AppEventService) { }

  async ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.studentId = params.id;
      // load part-time info
      await this.loadPartTimeInfo();
    });
    this.fragmentSubscription = this._activatedRoute.fragment.subscribe(async (fragment) => {
      if (fragment && fragment === 'reload') {
        // load part-time info
        await this.loadPartTimeInfo();
        // fire actions reload event
        this._appEventService.change.next({
          model: 'RefreshStudentActions',
          target: this.studentId
        });
      }
    });

    this.reloadSubscription = this._appEventService.changed.subscribe(async (change) => {
      if (change && change.model === 'StudentPartTimes' && change.target === this.studentId) {
        // load part-time-info
        await this.loadPartTimeInfo();
      }
    });
  }

  async loadPartTimeInfo() {
    this.lastPartTime = await this._context.model('StudentPartTimes')
      .where('student').equal(this.studentId)
      .expand('studyProgram,integrationStudyProgram,action($expand=partTimeCategory),student($expand=studentStatus,department)')
      .orderByDescending('startYear')
      .getItems();

    this.lastPartTime.map(item => {
          item.status = item.integrationStudyProgram ? 'Completed' :
            ((item.endYear.id === item.student.department.currentYear && item.endPeriod.id < item.student.department.currentPeriod)
              || (item.endYear.id < item.student.department.currentYear)) ? 'Expired' : 'InProgress';
    });
    if (this.lastPartTime && this.lastPartTime.length) {
      // get latest part-time info
      const lastPartTime = this.lastPartTime[0];
      // append isDeletable property
      this.inferIsDeletable(lastPartTime);
    }
  }

  inferIsDeletable(partTime: {reintegrated: boolean | number, student: {studentStatus: {alternateName: string}}}) {
    if (partTime == null) {
      return;
    }
    // latest part-time is deletable
    Object.defineProperty(partTime, 'isDeletable', {
      configurable: false,
      writable: false,
      enumerable: true,
      value:
        partTime.student &&
        partTime.student.studentStatus &&
        partTime.student.studentStatus.alternateName === 'active',
    });
  }

  async deleteStudentPartTime(partTime: {id: string, isDeletable: boolean}) {
    try {
      // validate partTime
      if (!(partTime && partTime.isDeletable)) {
        return Promise.resolve();
      }
      // fetch action translations
      const actionTranslations: {
        Description: string;
        ModalMessage: string;
        ToastMessage: string;
      } = this._translateService.instant('Students.PartTime.DeleteAction');
      // get user confirmation for deletion
      const dialogResult = await this._modalService.showWarningDialog(actionTranslations.Description,
        actionTranslations.ModalMessage,
        DIALOG_BUTTONS.OkCancel);
      if (dialogResult !== 'ok') {
        return Promise.resolve();
      }
      // show loading
      this._loadingService.showLoading();
      // execute deletion
      await this._context.model('StudentPartTimes').save({id: partTime.id, $state: 4});
      // hide loading
      this._loadingService.hideLoading();
      // show success toast message
      this._toastService.show(actionTranslations.Description, actionTranslations.ToastMessage);
      // and fire reload events
      // first local reload event
      this._appEventService.change.next({
        model: 'StudentPartTimes',
        target: this.studentId
      });
      // and finally actions/preview reload event
      this._appEventService.change.next({
        model: 'StudentPartTimes'
      });
      return;
    } catch (err) {
      this._errorService.showError(err, {
        continueLink: '.'
      });
    } finally {
      // ensure loading is hidden
      this._loadingService.hideLoading();
    }
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
    if (this.reloadSubscription) {
      this.reloadSubscription.unsubscribe();
    }
  }
}
