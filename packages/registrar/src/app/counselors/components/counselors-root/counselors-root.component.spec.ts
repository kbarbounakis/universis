import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CounselorsRootComponent } from './counselors-root.component';

describe('CounselorsRootComponent', () => {
  let component: CounselorsRootComponent;
  let fixture: ComponentFixture<CounselorsRootComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CounselorsRootComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CounselorsRootComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
