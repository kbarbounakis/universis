import {Component, Input, OnInit, OnDestroy, ViewChild, Output, EventEmitter} from '@angular/core';
import {
  AdvancedRowActionComponent,
  AdvancedTableComponent,
  AdvancedTableDataResult,
  AdvancedSearchFormComponent,
  AdvancedTableSearchComponent,
  ActivatedTableService,
  TableColumnConfiguration
} from '@universis/ngx-tables';
import { Router, ActivatedRoute } from '@angular/router';
import {Observable, Subscription} from 'rxjs';
import {UserActivityService, AppEventService, LoadingService, ErrorService, ModalService} from '@universis/common';
import { TranslateService } from '@ngx-translate/core';
import {ClientDataQueryable} from '@themost/client';
import {AngularDataContext} from '@themost/angular';
import { STUDENT_SHARED_COLUMNS as studentSharedColumns } from '../../../students/components/students-table/students-shared-columns-config';

@Component({
  selector: 'app-internships-table',
  templateUrl: './internships-table.component.html',
  styleUrls: ['./internships-table.component.scss']
})
export class InternshipsTableComponent implements OnInit, OnDestroy {

  private dataSubscription: Subscription;
  private paramSubscription: Subscription;

  @Input() tableConfiguration: any;
  @Input() searchConfiguration: any;

  @ViewChild('table') table: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  @ViewChild('advancedSearch') advancedSearch: AdvancedTableSearchComponent;
  @Output() refreshAction: EventEmitter<any> = new EventEmitter<any>();

  private selectedItems: any;

  public recordsTotal: any;
  fragmentSubscription: Subscription;

  onSearchKeyDown(event: any) {
    if (event.keyCode === 13) {
      this.table.search((<HTMLInputElement>event.target).value);
      return false;
    }
  }

  constructor(private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _activatedTable: ActivatedTableService,
    private _userActivityService: UserActivityService,
    private _translateService: TranslateService,
    private _appEvent: AppEventService,
    private _context: AngularDataContext,
    private _loadingService: LoadingService,
    private _errorService: ErrorService,
    private _modalService: ModalService
  ) {
  }

  ngOnInit() {
    this.dataSubscription = this._activatedRoute.data.subscribe(data => {
      this._activatedTable.activeTable = this.table;
      // set search form
      if (data.searchConfiguration) {
        this.search.form = data.searchConfiguration;
        this.search.ngOnInit();
      }
      // set table config and recall data
      if (data.tableConfiguration) {
        // set config
        this.table.config = data.tableConfiguration;
        // reset search text
        this.advancedSearch.text = null;
        // reset table
        this.table.reset(false);
      }

      this._userActivityService.setItem({
        category: this._translateService.instant('Internships.Title'),
        description: this._translateService.instant(this.table.config.title),
        url: window.location.hash.substring(1), // get the path after the hash
        dateCreated: new Date
      });

      // this.fragmentSubscription = this._activatedRoute.fragment.subscribe((fragment) => {
      //   if (fragment === 'reload') {
      //     this.table.fetch(true);
      //   }
      // });

    });
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  async getSelectedItems() {
    let items = [];
    if (this.table && this.table.lastQuery) {
      const lastQuery: ClientDataQueryable = this.table.lastQuery;
      if (lastQuery != null) {
        if (this.table.smartSelect) {
          // get items
          const selectArguments = ['id'];
          // query items
          const queryItems = await lastQuery.select.apply(lastQuery, selectArguments)
            .take(-1)
            .skip(0)
            .getItems();
          if (this.table.unselected && this.table.unselected.length) {
            // remove items that have been unselected by the user
            items = queryItems.filter( item => {
              return this.table.unselected.findIndex( (x) => {
                return x.id === item.id;
              }) < 0;
            });
          } else {
            items = queryItems;
          }
        } else {
          // get selected items only
          items = this.table.selected;
        }
      }
    }
    return items;
  }


  /**
   * Deletes selected Interships
   */
  async deleteAction() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      // filter out courses parts
      this.selectedItems = items.filter(
        item => item.statusAlternateName !== 'completed' &&
        ( !item.student || item.studentStatusAlternateName === 'active')
      );
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Internships.DeleteAction.Title',
          description: 'Internships.DeleteAction.Description',
          errorMessage: 'Internships.DeleteAction.CompletedWithErrors',
          refresh: this.refreshAction,
          execute: this.executeDeleteAction()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  /**
   * Executes delete action for Theses
   */
  executeDeleteAction() {
    return new Observable((observer) => {
      this.refreshAction.emit({
        progress: 1
      });
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      // execute update for all items (transactional update)
      // map items
      const updated = this.selectedItems.map((item) => {
        return {
          id: item.id
        };
      });
      // handle fake progress with interval
      let progressValue = 5;
      const progressInterval = setInterval(() => {
        progressValue = progressValue + 10 < 100 ? progressValue + 10 : 5;
        this.refreshAction.emit({
          progress: progressValue
        });
      }, 1000);
      this._context.model('Internships').remove(updated).then(() => {
        // reload table
        this.table.fetch(true);
        // stop progress
        if (progressInterval) {
          clearInterval(progressInterval);
        }
        // and return result
        return observer.next(result);
      }).catch((err) => {
        // stop progress
        if (progressInterval) {
          clearInterval(progressInterval);
        }
        // and return result with errors
        result.errors = result.total;
        return observer.next(result);
      });
    });
  }

  onLoading(event: { target: AdvancedTableComponent }) {
    if (event && event.target && event.target.config) {
      // clone shared columns
      const clonedSharedColumns = JSON.parse(JSON.stringify(studentSharedColumns));
      const configColumns = event.target.config.columns || [];
      clonedSharedColumns.forEach((column: TableColumnConfiguration) => {
        column.name = `student/${column.name}`;
        // ensure shared config's properies start with model's name
        if (!/^student/.test(column.property)){
          column.property = `student${column.property[0].toUpperCase() + column.property.slice(1)}`;
        }
        // try to find column in the config by name
        const findColumn = configColumns.find((configColumn) => configColumn.name === column.name);
        // if it does not exist
        if (findColumn == null) {
          // push it
          configColumns.push(column);
        }
      });
    }
  }
}
