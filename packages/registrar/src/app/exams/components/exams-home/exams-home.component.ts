import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription, combineLatest } from 'rxjs';
import { TableConfigurationResolver } from '../../../registrar-shared/table-configuration.resolvers';

declare interface TablePathConfiguration {
  name: string;
  alternateName: string;
  show?: boolean;
}

@Component({
  selector: 'app-exams-home',
  templateUrl: './exams-home.component.html',
  styleUrls: ['./exams-home.component.scss']
})
export class ExamsHomeComponent implements OnInit {

  public paths: Array<TablePathConfiguration> = [];
  public activePaths: Array<TablePathConfiguration> = [];
  public maxActiveTabs = 3;
  public tabs: Array<any> = [];
  private paramSubscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute, private _resolver: TableConfigurationResolver) { }

  ngOnInit() {

    this.paramSubscription = combineLatest(
      this._resolver.get('CourseExams'),
      this._activatedRoute.firstChild.params
    ).subscribe( (results) => {
      const config: any = results[0];
      const params = results[1];
      if (this.paths.length === 0) {
        this.paths = config.paths;
        this.activePaths = this.paths.filter( x => {
          return x.show === true;
        }).slice(0, this.maxActiveTabs);
      }
      const matchPath = new RegExp('^list/' + params.list  + '$', 'ig');
      const findItem = this.paths.find( x => {
        return matchPath.test(x.alternateName);
      });
      if (findItem) {
        this.showTab(findItem);
      }
    });

  }

  showTab(item: any) {
    // find if path exists in active paths
    const findIndex = this.activePaths.findIndex( x => {
      return x.alternateName === item.alternateName;
    });
    if (findIndex < 0) {
      if (this.activePaths.length >= this.maxActiveTabs) {
        // remove item at index 0
        this.activePaths.splice(0, 1);
      }
      // add active path
      this.activePaths.push(Object.assign({}, item));
    }
  }

  ngOnDestroy(): void {
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
  }

}
