import { TableColumnConfiguration } from "@universis/ngx-tables";

export const EXAM_SHARED_COLUMNS: TableColumnConfiguration[] = [
  {
    name: "id",
    property: "id",
    title: "Exams.SharedColumns.id",
    hidden: true,
    optional: true
  },
  {
    name: "description",
    property: "description",
    title: "Exams.SharedColumns.description",
    hidden: true,
    optional: true
  },
  {
    name: "notes",
    property: "notes",
    title: "Exams.SharedColumns.notes",
    hidden: true,
    optional: true
  },
  {
    name: "isLate",
    property: "isLate",
    title: "Exams.SharedColumns.isLate",
    hidden: true,
    optional: true
  },
  {
    name: "examDate",
    property: "examDate",
    title: "Exams.SharedColumns.examDate",
    hidden: true,
    optional: true
  },
  {
    name: "status/alternateName",
    property: "examStatus",
    title: "Exams.SharedColumns.status",
    hidden: true,
    optional: true
  },
  {
    name: "isCalculated",
    property: "isCalculated",
    title: "Exams.SharedColumns.isCalculated",
    hidden: true,
    optional: true
  },
  {
    name: "decimalDigits",
    property: "decimalDigits",
    title: "Exams.SharedColumns.decimalDigits",
    hidden: true,
    optional: true
  },
  {
    name: "name",
    property: "name",
    title: "Exams.SharedColumns.name",
    hidden: true,
    optional: true
  },
  {
    name: "resultDate",
    property: "resultDate",
    title: "Exams.SharedColumns.resultDate",
    hidden: true,
    optional: true
  },
  {
    name: "numberOfGradedStudents",
    property: "numberOfGradedStudents",
    title: "Exams.SharedColumns.numberOfGradedStudents",
    hidden: true,
    optional: true
  },
  {
    name: "dateCompleted",
    property: "dateCompleted",
    title: "Exams.SharedColumns.dateCompleted",
    hidden: true,
    optional: true
  },
  {
    name: "dateModified",
    property: "dateModified",
    title: "Exams.SharedColumns.dateModified",
    hidden: true,
    optional: true
  },
  {
    name: "gradeScale/name",
    property: "gradeScale",
    title: "Exams.SharedColumns.gradeScale",
    hidden: true,
    optional: true
  },
  {
    name: "types/name",
    property: "examType",
    title: "Exams.SharedColumns.types",
    hidden: true,
    optional: true
  },
]