import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExamsPreviewComponent } from './exams-preview.component';

describe('ExamsPreviewComponent', () => {
  let component: ExamsPreviewComponent;
  let fixture: ComponentFixture<ExamsPreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExamsPreviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExamsPreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
