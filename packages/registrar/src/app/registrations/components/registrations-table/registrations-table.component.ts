import { Component, OnInit, ViewChild, Input, OnDestroy, Output, EventEmitter, AfterViewInit } from '@angular/core';
import * as REGISTRATIONS_LIST_CONFIG from './registrations-table.config.list.json';
import {
  AdvancedTableComponent,
  AdvancedTableConfiguration,
  AdvancedTableDataResult,
  AdvancedTableSearchComponent,
  AdvancedSearchFormComponent,
  ActivatedTableService,
  AdvancedRowActionComponent,
  TableColumnConfiguration
} from '@universis/ngx-tables';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { AppEventService, ErrorService, LoadingService, ModalService, ToastService } from '@universis/common';
import { AngularDataContext } from '@themost/angular';
import { ClientDataQueryable } from '@themost/client';
import { ActiveDepartmentService } from '../../../registrar-shared/services/activeDepartmentService.service';
import { ActiveTableQueryBuilder } from '../../../query-builder/active-table-query-builder';
import { STUDENT_SHARED_COLUMNS as studentSharedColumns } from '../../../students/components/students-table/students-shared-columns-config';


enum AutoRegisterClassActionType {
  OnlyObligatory = 'onlyObligatory',
  NotPassedObligatory = 'notPassedObligatory',
  NotPassed = 'notPassed',
  SpecificSelection = 'specificSelection'
}

@Component({
  selector: 'app-registrations-table',
  templateUrl: './registrations-table.component.html',
  styleUrls: ['./registrations-table.component.scss']
})
export class RegistrationsTableComponent implements OnInit, OnDestroy, AfterViewInit {


  public readonly config = REGISTRATIONS_LIST_CONFIG;
  public recordsTotal: any;
  private selectedItems: any;
  private dataSubscription: Subscription;
  private _batchSize = 100;
  public activeDepartment: any;
  @Input() tableConfiguration: any;
  @Input() searchConfiguration: any;
  @ViewChild('table') table: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  @ViewChild('advancedSearch') advancedSearch: AdvancedTableSearchComponent;
  @Output() refreshAction: EventEmitter<any> = new EventEmitter<any>();

  queryDescription: string;
  routeSubscription: Subscription;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _activatedTable: ActivatedTableService,
    private _translateService: TranslateService,
    private _modalService: ModalService,
    private _loadingService: LoadingService,
    private _errorService: ErrorService,
    private _context: AngularDataContext,
    private _activeDepartment: ActiveDepartmentService,
    private _appEvent: AppEventService,
    public _activeQuery: ActiveTableQueryBuilder,
    public _toastService: ToastService,
    public _router: Router
  ) { }


  ngAfterViewInit(): void {
    this.routeSubscription = this._activatedRoute.queryParams.subscribe((queryParams) => {
      this._activatedTable.activeTable = this.table;
      this._activeQuery.find(queryParams).subscribe((item) => {
        this.queryDescription = item && item.description;
      });
    });
  }


  ngOnInit() {
    this.dataSubscription = this._activatedRoute.data.subscribe(async data => {
      this._appEvent.change.next({
        model: 'ToggleShowExport',
        target: true
      });
      this._activatedTable.activeTable = this.table;
      if (data.searchConfiguration) {
        this.search.form = data.searchConfiguration;
        // get active department
        this.activeDepartment = await this._activeDepartment.getActiveDepartment();
        // assign id to form
        Object.assign(this.search.form, { department: this.activeDepartment && this.activeDepartment.id });
        this.search.ngOnInit();
      }
      if (data.tableConfiguration) {
        const config = data.tableConfiguration;
        // use custom method for getting students without registration
        if (config.title === 'Students.Active') {
          this._appEvent.change.next({
            model: 'ToggleShowExport',
            target: false
          });
          // set config model
          config.model = `Students/withoutCurrentRegistration?department=${this.activeDepartment && this.activeDepartment.id}`;
        }
        // append config to table
        this.table.config = AdvancedTableConfiguration.cast(config);
        // reset search text
        this.advancedSearch.text = null;
        // reset table
        this.table.reset(false);
      }
    });

  }

  async onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
    if (this.routeSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }

  /*private async takeSelectedItems(take?: number, skip?: number) {
    let items = {
      total: 0,
      skip: 0,
      value: []
    };
    if (this.table && this.table.lastQuery) {
      const lastQuery: ClientDataQueryable = this.table.lastQuery;
      if (lastQuery != null) {
        if (this.table.smartSelect) {
          let smartTake = -1;
          let smartSkip = 0;
          if (typeof take === 'number') {
            smartTake = take;
          }
          if (typeof skip === 'number') {
            smartSkip = skip;
          }
          // get items
          const selectArguments = ['id',
              'status/alternateName as status',
              'registrationYear/id as registrationYear',
              'registrationPeriod/id as registrationPeriod'
          ];
          // query items
          const queryItems = await lastQuery.select.apply(lastQuery, selectArguments)
              .take(smartTake)
              .skip(smartSkip)
              .getList();
          if (this.table.unselected && this.table.unselected.length) {
            // remove items that have been unselected by the user
            items = {
              total: queryItems.total - this.table.unselected.length,
              skip: smartSkip,
              value: queryItems.value.filter( item => {
                return this.table.unselected.findIndex( (x) => {
                  return x.id === item.id;
                }) < 0;
              })
            };
          } else {
            items = queryItems;
          }
        } else {
          // get selected items only
          items = {
            total: this.table.selected.length,
            skip: 0,
            value: this.table.selected.map( (item) => {
              return {
                id: item.id,
                status: item.status,
                registrationYear: item.registrationYear,
                registrationPeriod: item.registrationPeriod
              };
            })
          };
        }
      }
    }
    return items;
  }*/

  /**
   * Executes open action for course classes
   */
  executeChangeStatusAction(status) {
    return new Observable((observer) => {
      this.refreshAction.emit({
        progress: 1
      });
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      // execute promises in series within an async method
      (async () => {
        // prepare the items by assigning the new status
        const updateStatuses = this.selectedItems.map((item: any) => {
          return {
            id: item.id,
            status: {
              alternateName: status
            },
            registrationYear: item.registrationYear,
            registrationPeriod: item.registrationPeriod
          };
        });
        // start creating batches of registrations
        // based on a fixed size
        let batchIndex = 0;
        const totalItems = updateStatuses.length;
        do {
          // get next batch (use slice-not splice-to prevent change on original array)
          const updateStatusBatch = updateStatuses.slice(batchIndex, batchIndex + this._batchSize);
          try {
            // update status
            await this._context.model('StudentPeriodRegistrations').save(updateStatusBatch);
            // and update success counter
            result.success += updateStatusBatch.length;
          } catch (err) {
            // log error
            console.error(err);
            // and update error counter
            result.errors += updateStatusBatch.length;
          }
          // update progress
          this.refreshAction.emit({
            progress: Math.floor(((batchIndex + this._batchSize) / totalItems) * 100)
          });
          // and update index
          batchIndex += this._batchSize;
        } while (batchIndex < totalItems);
      })().then(() => {
        // reload table
        this.table.fetch(true);
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  async openAction() {
    try {
      // validate search filter (on smart selection)
      if (this.table.smartSelect) {
        const filterStatus = this.search.filter && this.search.filter.status;
        if (filterStatus == null || ['open', 'pending'].includes(filterStatus)) {
          const error = this._translateService.instant('Registrations.OpenActionFilter');
          return this._modalService.showDialog(error.title, error.message);
        }
        if (this.table.config && this.table.config['id'] === 'registrations-index-list') {
          // validate registration year and period filters when on index (all) list
          const yearFilter = this.search.filter && this.search.filter.registrationYear;
          const periodFilter = this.search.filter && this.search.filter.registrationPeriod;
          if (!yearFilter || !periodFilter) {
            const error = this._translateService.instant('Registrations.MassActionYearPeriodFilter');
            return this._modalService.showDialog(error.title, error.message);
          }
        }
      }
      this._loadingService.showLoading();
      // fetch and filter selected items
      const items = await this.getSelectedItems();
      this.selectedItems = items.filter((item: any) =>
        // consider only items of closed status
        item.status === 'closed' &&
        // and of active students
        item.studentStatus === 'active');
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Registrations.OpenAction.Title',
          description: 'Registrations.OpenAction.Description',
          refresh: this.refreshAction,
          execute: this.executeChangeStatusAction('open')
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  async closeAction() {
    try {
      // validate search filter (on smart selection)
      if (this.table.smartSelect) {
        const filterStatus = this.search.filter && this.search.filter.status;
        if (filterStatus == null || filterStatus === 'closed') {
          const error = this._translateService.instant('Registrations.CloseActionFilter');
          return this._modalService.showDialog(error.title, error.message);
        }
        if (this.table.config && this.table.config['id'] === 'registrations-index-list') {
          // validate registration year and period filters when on index (all) list
          const yearFilter = this.search.filter && this.search.filter.registrationYear;
          const periodFilter = this.search.filter && this.search.filter.registrationPeriod;
          if (!yearFilter || !periodFilter) {
            const error = this._translateService.instant('Registrations.MassActionYearPeriodFilter');
            return this._modalService.showDialog(error.title, error.message);
          }
        }
      }
      this._loadingService.showLoading();
      // fetch and filter selected items
      const items = await this.getSelectedItems();
      this.selectedItems = items.filter((item: any) =>
        // consider items of open/pending status
        ['open', 'pending'].includes(item.status));
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Registrations.CloseAction.Title',
          description: 'Registrations.CloseAction.Description',
          refresh: this.refreshAction,
          execute: this.executeChangeStatusAction('closed')
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  async deleteAction() {
    try {
      // validate search filter (on smart selection)
      if (this.table.smartSelect) {
        if (this.table.config && this.table.config['id'] === 'registrations-index-list') {
          // validate registration year and period filters when on index (all) list
          const yearFilter = this.search.filter && this.search.filter.registrationYear;
          const periodFilter = this.search.filter && this.search.filter.registrationPeriod;
          if (!yearFilter || !periodFilter) {
            const error = this._translateService.instant('Registrations.MassActionYearPeriodFilter');
            return this._modalService.showDialog(error.title, error.message);
          }
        }
      }
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      // filter out items with classes or history
      this.selectedItems = items.filter(item => {
        // consider items with no classes
        const totalClasses = item.classes && item.classes.length && item.classes[0].totalClasses;
        // and of active students
        const activeStudent = item.studentStatus === 'active';
        return (!totalClasses || totalClasses === 0)  && (activeStudent);
      });
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Registrations.DeleteAction.Title',
          description: 'Registrations.DeleteAction.Description',
          errorMessage: 'Registrations.DeleteAction.CompletedWithErrors',
          refresh: this.refreshAction,
          execute: this.executeDeleteAction()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  executeDeleteAction() {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      this.refreshAction.emit({
        progress: 1
      });
      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            const item = this.selectedItems[index];
            // assign delete state
            Object.defineProperty(item, '$state', {
              configurable: true,
              writable: true,
              enumerable: true,
              value: 4
            });
            // and save
            await this._context.model('StudentPeriodRegistrations').save(item);
            result.success += 1;
          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }
        }
      })().then(() => {
        // reload table on success / fetchOne is not needed for this action
        this.table.fetch(true);
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  async createRegistrationAction() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      // filter out items with classes or history
      this.selectedItems = items.filter(item => {
        const hasCurrentRegistration = item.registrations && item.registrations.length && item.registrations[0].hasCurrentRegistration;
        return !hasCurrentRegistration || hasCurrentRegistration === 0;
      });
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Registrations.CreateRegistrationAction.Title',
          description: 'Registrations.CreateRegistrationAction.Description',
          errorMessage: 'Registrations.CreateRegistrationAction.CompletedWithErrors',
          refresh: this.refreshAction,
          execute: this.executeCreateRegistrationAction()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  executeCreateRegistrationAction() {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      this.refreshAction.emit({
        progress: 1
      });
      // execute promises in series within an async method
      (async () => {
        // always get currentYear, currentPeriod from db
        // ignore local cache
        // refetch the department on-action-click
        const department = await this._context.model('LocalDepartments')
          .where('id').equal(this.activeDepartment && this.activeDepartment.id)
          .select('currentYear/id as currentYear', 'currentPeriod/id as currentPeriod')
          .getItem();
        if (!(department && department.currentYear && department.currentPeriod)) {
          return Promise.resolve();
        }
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            const item = this.selectedItems[index];
            const newRegistration = {
              student: item.id,
              registrationYear: department.currentYear,
              registrationPeriod: department.currentPeriod,
              registrationDate: new Date(),
              status: {
                alternateName: 'open'
              }
            };
            // recheck if student has registration
            const existingRegistration = await this._context.model('StudentPeriodRegistrations')
              .where('student').equal(newRegistration.student)
              .and('registrationYear').equal(newRegistration.registrationYear)
              .and('registrationPeriod').equal(newRegistration.registrationPeriod)
              .select('id')
              .getItem();
            // and throw error
            if (existingRegistration) {
              throw new Error(this._translateService.instant('Registrations.CreateRegistrationAction.RegistrationExists'));
            }
            // and save
            const registrationResult = await this._context.model(`Students/${item.id}/registrations`).save(newRegistration);
            // catch validation result error
            if (registrationResult && registrationResult.validationResult && registrationResult.validationResult.success === false) {
              throw new Error(registrationResult.validationResult.innerMessage || registrationResult.validationResult.message);
            }
            result.success += 1;
          } catch (err) {
            // log error
            console.error(err);
            result.errors += 1;
          }
        }
      })().then(() => {
        this.table.fetch(true);
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  async changeRegistrationType() {
    try {
      // validate search filter (on smart selection)
      if (this.table.smartSelect) {
        const registrationTypeFilter = this.search.filter && this.search.filter.registrationType;
        const studentStatusfilter = this.search.filter && this.search.filter.studentStatus;
        const statusfilter = this.search.filter && this.search.filter.status;
        if (!registrationTypeFilter || !studentStatusfilter || !statusfilter) {
          const error = this._translateService.instant('Registrations.ChangePeriodRegistrationType');
          return this._modalService.showDialog(error.title, error.message);
        }
        if (this.table.selected.length > 100) {
          const error = this._translateService.instant('Registrations.ChangePeriodRegistrationTypeTooManyRecords');
          return this._modalService.showDialog(error.title, error.message);
        }
      }

      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      const filteredItems = items.filter((item: any) =>
        // consider only items of open status
        item.status === 'open' &&
        // and of active students
        item.studentStatus === 'active');
      if (filteredItems.length < items.length) {
        this.selectedItems = [];
      } else {
        this.selectedItems = items;
      }
      // fetch and filter selected items
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          formTemplate: this.selectedItems && this.selectedItems.length > 0 ? 'StudentPeriodRegistrations/changeRegistrationType' : null,
          modalTitle: 'Registrations.ChangePeriodRegistrationTypeAction.Title',
          description: 'Registrations.ChangePeriodRegistrationTypeAction.Description',
          additionalMessage: this._translateService.instant('Registrations.ChangePeriodRegistrationTypeAction.NoItems'),
          refresh: this.refreshAction,
          execute: this.executeChangeRegistrationType()
        }
      });

    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  executeChangeRegistrationType() {
    return new Observable((observer) => {
      this.refreshAction.emit({
        progress: 1
      });
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      const component = this._modalService.modalRef.content as AdvancedRowActionComponent;
      const data = component.formComponent.form.formio.data;
      if (data.registrationType == null) {
        this.selectedItems = [];
        result.errors = result.total;
        return observer.next(result);
      }
      // execute promises in series within an async method
      (async () => {
        // prepare the items by assigning the new status
        const updateRegistrationTypes = this.selectedItems.map((item: any) => {
          return {
            id: item.id,
            registrationType: data.registrationType,
            registrationYear: item.registrationYear,
            registrationPeriod: item.registrationPeriod
          };
        });
        // start creating batches of registrations
        // based on a fixed size
        let batchIndex = 0;
        const totalItems = updateRegistrationTypes.length;
        do {
          // get next batch (use slice-not splice-to prevent change on original array)
          const updateRegistrationTypesBatch = updateRegistrationTypes.slice(batchIndex, batchIndex + this._batchSize);
          try {
            // update status
            await this._context.model('StudentPeriodRegistrations').save(updateRegistrationTypesBatch);
            // and update success counter
            result.success += updateRegistrationTypesBatch.length;
          } catch (err) {
            // log error
            console.error(err);
            // and update error counter
            result.errors += updateRegistrationTypesBatch.length;
          }
          // update progress
          this.refreshAction.emit({
            progress: Math.floor(((batchIndex + this._batchSize) / totalItems) * 100)
          });
          // and update index
          batchIndex += this._batchSize;
        } while (batchIndex < totalItems);
      })().then(() => {
        // reload table
        this.table.fetch(true);
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  async getSelectedItems() {
    let items = [];
    if (this.table && this.table.lastQuery) {
      const lastQuery: ClientDataQueryable = this.table.lastQuery;
      if (lastQuery != null) {
        if (this.table.smartSelect) {
          // select only id for students table
          const selectArguments = [
            'id'
          ];
          if (this.table && this.table.config && this.table.config.title !== 'Students.Active') {
            // push extra select arguments for registration tables
            Array.prototype.push.apply(selectArguments,
              [
                'status/alternateName as status',
                'registrationYear/id as registrationYear',
                'registrationPeriod/id as registrationPeriod',
                'student/studentStatus/alternateName as studentStatus',
                'student'
              ]);
          }
          // query items
          const queryItems = await lastQuery.select.apply(lastQuery, selectArguments)
            .take(-1)
            .skip(0)
            .getItems();
          if (this.table.unselected && this.table.unselected.length) {
            // remove items that have been unselected by the user
            items = queryItems.filter(item => {
              return this.table.unselected.findIndex((x) => {
                return x.id === item.id;
              }) < 0;
            });
          } else {
            items = queryItems;
          }
        } else {
          // get selected items only
          items = this.table.selected;
        }
      }
    }
    return items;
  }

  async autoRegisterClassesAction() {
    try {
      this._loadingService.showLoading();
      // validate in progress action
      const inProgressAction = await this._context.model('AutoRegisterClassActions')
        .where('department').equal(this.activeDepartment && this.activeDepartment.id)
        .and('actionStatus/alternateName').equal('ActiveActionStatus')
        .select('id')
        .getItem();
      if (inProgressAction) {
        // show toast message
        const toastData: {
          ToastTitle: string,
          ToastMessage: string
        } = this._translateService.instant('Registrations.AutoRegisterClassesAction.InProgressAction');
        this._toastService.show(toastData.ToastTitle, toastData.ToastMessage);
        // and exit
        return this._router.navigate(['departments', 'current', 'automated-registrations', 'results']);
      }
      const items = await this.getSelectedItems();
      // apply filter for registrations
      if (this.table.config && this.table.config.title !== 'Students.Active') {
        this.selectedItems = items.filter((registration: any) => {
          // consider open registrations of active students
          return registration.status === 'open' && registration.studentStatus === 'active';
        });
      } else {
        // filter is already applied for students
        this.selectedItems = items;
      }
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          formTemplate: this.selectedItems && this.selectedItems.length > 0 ? 'AutoRegisterClassActions/configure' : null,
          data: { 'activeDepartment': this.activeDepartment },
          modalTitle: 'Registrations.AutoRegisterClassesAction.Title',
          description: this.table.config && this.table.config.title !== 'Students.Active'
            ? 'Registrations.AutoRegisterClassesAction.DescriptionRegistrations'
            : 'Registrations.AutoRegisterClassesAction.DescriptionStudents',
          errorMessage: this.table.config && this.table.config.title !== 'Students.Active'
            ? 'Registrations.AutoRegisterClassesAction.CompletedWithErrorsRegistrations'
            : 'Registrations.AutoRegisterClassesAction.CompletedWithErrorsStudents',
          refresh: this.refreshAction,
          execute: this.executeAutoRegisterClassesAction()
        }
      });
    } catch (err) {
      this._errorService.showError(err, {
        continueLink: '.'
      });
    } finally {
      this._loadingService.hideLoading();
    }
  }

  executeAutoRegisterClassesAction() {
    return new Observable((observer) => {
      const abortAction = (resultObject: any, intervalObject: any) => {
        if (intervalObject) {
          clearInterval(intervalObject);
        }
        this.selectedItems = [];
        resultObject.errors = resultObject.total;
        return observer.next(resultObject);
      };
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      // get form data
      const component = this._modalService.modalRef.content as AdvancedRowActionComponent;
      const data = component
        && component.formComponent
        && component.formComponent.form
        && component.formComponent.form.formio
        && component.formComponent.form.formio.data;
      // validate autoRegisterClassActionType
      if (!(data && data.autoRegisterClassActionType)) {
        abortAction(result, null);
      }
      this.refreshAction.emit({
        progress: 1
      });
      // handle fake progress with interval
      let progressValue = 5;
      const progressInterval = setInterval(() => {
        progressValue = progressValue + 10 < 100 ? progressValue + 10 : 5;
        this.refreshAction.emit({
          progress: progressValue
        });
      }, 1000);
      // prepare action data
      const actionData = {
        students: this.selectedItems.map((item: any) => item.student || item.id),
        data: {
          department: this.activeDepartment && this.activeDepartment.id
        }
      };
      // switch on auto autoRegisterClassActionType
      switch (data.autoRegisterClassActionType) {
        case AutoRegisterClassActionType.OnlyObligatory: {
          // define only obligatory attribute and set its value to true
          Object.defineProperty(actionData.data, 'onlyObligatory', {
            configurable: true,
            enumerable: true,
            writable: true,
            value: true
          });
          break;
        }
        case AutoRegisterClassActionType.NotPassed: {
          // validate course type
          if (!data.obligatoryCourseType) {
            abortAction(result, progressInterval);
          }
          // and define the attribute
          Object.defineProperty(actionData.data, 'courseType', {
            configurable: true,
            enumerable: true,
            writable: true,
            value: data.obligatoryCourseType
          });
          break;
        }
        case AutoRegisterClassActionType.NotPassedObligatory: {
          // define the attribute and set its value to true
          Object.defineProperty(actionData.data, 'notPassedObligatory', {
            configurable: true,
            enumerable: true,
            writable: true,
            value: true
          });
          break;
        }
        case AutoRegisterClassActionType.SpecificSelection: {
          // validate course classes
          if (!(Array.isArray(data.courseClasses) && data.courseClasses.length)) {
            abortAction(result, progressInterval);
          }
          // and define the attribute
          Object.defineProperty(actionData.data, 'courseClasses', {
            configurable: true,
            enumerable: true,
            writable: true,
            value: data.courseClasses
          });
          break;
        }
        default:
          abortAction(result, progressInterval);
      }
      // and start the async process
      this._context.model('Students/autoRegisterClasses').save(actionData).then(() => {
        // stop progress
        if (progressInterval) {
          clearInterval(progressInterval);
        }
        // show toast message
        const toastData: {
          ToastTitle: string,
          ToastMessage: string
        } = this._translateService.instant('Registrations.AutoRegisterClassesAction.OnSuccess');
        this._toastService.show(toastData.ToastTitle, toastData.ToastMessage, true, 6000);
        // dispatch result
        observer.next(result);
        // and navigate to results page
        return this._router.navigate(['departments', 'current', 'automated-registrations', 'results']);
      }).catch((err) => {
        console.error(err);
        // stop progress
        if (progressInterval) {
          clearInterval(progressInterval);
        }
        // and return result with errors
        result.errors = result.total;
        return observer.next(result);
      });
    });
  }

  onLoading(event: { target: AdvancedTableComponent }) {
    if (event && event.target && event.target.config) {
      // clone shared columns
      const clonedSharedColumns = JSON.parse(JSON.stringify(studentSharedColumns));
      const configColumns = event.target.config.columns || [];
      clonedSharedColumns.forEach((column: TableColumnConfiguration) => {
        if (event.target.config.title !== "Students.Active") {
          column.name = `student/${column.name}`;
        }
        // ensure shared config's properies start with model's name
        if (!/^student/.test(column.property)){
          column.property = `student${column.property[0].toUpperCase() + column.property.slice(1)}`;
        }
        // try to find column in the config by name
        const findColumn = configColumns.find((configColumn) => configColumn.name === column.name);
        // if it does not exist
        if (findColumn == null) {
          // push it
          configColumns.push(column);
          // add column name to groupBy string if there is one and the column is not virtual
          if (event.target.config.defaults && event.target.config.defaults.groupBy && column.virtual !== true) {
            event.target.config.defaults.groupBy += `,${column.name}`;
          }
        }
      });
    }
  }
}
