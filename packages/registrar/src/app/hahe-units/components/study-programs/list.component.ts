import { Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild, ViewEncapsulation } from '@angular/core';
import { AdvancedRowActionComponent, AdvancedSearchFormComponent,
  AdvancedTableComponent, AdvancedTableConfiguration, AdvancedTableDataResult } from '@universis/ngx-tables';
import * as StudyProgramTableConfiguration from './list.json';
import * as programSearchConfiguration from './search.json';
import { AppEventService, DIALOG_BUTTONS, ErrorService, LoadingService, ModalService } from '@universis/common';
import { Observable, Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { AngularDataContext } from '@themost/angular';
@Component({
  selector: 'app-hahe-units-programs-list',
  templateUrl: './list.component.html',
  encapsulation: ViewEncapsulation.None
})
export class StudyProgramsListComponent implements OnInit, OnDestroy {

  @ViewChild('table') table: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  @Output() refreshAction: EventEmitter<any> = new EventEmitter<any>();
  private selectedItems: any[];

  recordsTotal: number;
  addSubscription: Subscription;

  constructor(private appEvent: AppEventService,
              private modalService: ModalService,
              private errorService: ErrorService,
              private translateService: TranslateService,
              private context: AngularDataContext,
              private loadingService: LoadingService) {
    this.addSubscription = this.appEvent.added.subscribe((event: any) => {
      if (event && event.model === 'HaheUnits') {
        this.table.fetch();
      }
    });
  }

  ngOnDestroy(): void {
    if (this.addSubscription) {
      this.addSubscription.unsubscribe();
    }
  }

   ngOnInit() {
    this.table.config = AdvancedTableConfiguration.cast(StudyProgramTableConfiguration);
    this.search.form =   programSearchConfiguration;
    this.search.ngOnInit();
  }

  /**
   * Data load event handler of advanced table component
   * @param {AdvancedTableDataResult} data
   */
  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  setHaheUnitAction() {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      const component = this.modalService.modalRef.content as AdvancedRowActionComponent;
      const data = component.formComponent.form.formio.data;
      if (data.haheUnit == null) {
        this.selectedItems = [];
        result.errors = result.total;
        return observer.next(result);
      }
      this.refreshAction.emit({
        progress: 1
      });
      const selectedItems = this.selectedItems.map((item) => {
        return {
          id: item.id
        };
      });
      (async () => {
        let haheUnit = data.haheUnit;
        if (haheUnit === '') {
          haheUnit = null;
        }
        for (let index = 0; index < selectedItems.length; index++) {
          try {
            const item = selectedItems[index];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            await this.context.model('StudyPrograms').save({
              id: item.id,
              haheUnit: haheUnit
            });
            try {
              await this.table.fetchOne({
                id: item.id
              });
            } catch (err) {
              //
            }
          } catch (err) {
            // log error
            console.error(err);
            result.errors += 1;
          }
        }
        // clear selected
        this.table.selectNone();
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  setHaheUnit() {
   this.selectedItems = this.table.selected;
    this.modalService.openModalComponent(AdvancedRowActionComponent, {
      class: 'modal-lg',
      keyboard: false,
      ignoreBackdropClick: true,
      initialState: {
        items: this.selectedItems, // set items

        modalTitle: this.translateService.instant('HaheUnits.SetHaheUnitAction'), // set title
        description: this.translateService.instant('HaheUnits.SetHaheUnitDescription'),
        okButtonText: this.translateService.instant('Tables.Apply'),
        okButtonClass: 'btn btn-success',
        formTemplate: 'StudyPrograms/setHaheUnit',
        progressType: 'success',
        refresh: this.refreshAction, // refresh action event
        execute: this.setHaheUnitAction()
      }
    });
  }

  clearHaheUnit() {
    if (this.table && this.table.selected && this.table.selected.length) {
      // get items to remove
      const items = this.table.selected.map((item) => {
        return {
          id: item.id,
          haheUnit: null
        };
      });
      return this.modalService.showWarningDialog(
        this.translateService.instant('HaheUnits.ClearHaheUnitTitle'),
        this.translateService.instant('HaheUnits.ClearHaheUnitMessage'),
        DIALOG_BUTTONS.OkCancel).then(result => {
        if (result === 'ok') {
          this.loadingService.showLoading();
          this.context.model('StudyPrograms').save(items).then(() => {
            this.table.fetch(true);
            this.loadingService.hideLoading();
            this.loadingService.hideLoading();
          }).catch(err => {
            this.loadingService.hideLoading();
            this.errorService.showError(err, {
              continueLink: '.'
            });
          });
        }
      });
    }
  }
}
