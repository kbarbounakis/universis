import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AngularDataContext } from '@themost/angular';
import { LoadingService } from '@universis/common';
import { RouterModalOkCancel } from '@universis/common/routing';
import { Subscription } from 'rxjs';
import { groupBy } from 'lodash';

enum QueryOptions {
  All = -1
}

@Component({
  selector: 'app-digital-signature-details',
  templateUrl: './digital-signature-details.component.html',
  styles: []
})
export class DigitalSignatureDetailsComponent extends RouterModalOkCancel implements OnInit, OnDestroy {
  private subscription: Subscription;

  public lastError: string;
  public loading: boolean;
  public actions = [];
  public useSignaturePriority: boolean;

  constructor(
    protected router: Router,
    protected activatedRoute: ActivatedRoute,
    private translateService: TranslateService,
    private context: AngularDataContext,
    private loadingService: LoadingService
  ) {
    // call super constructor
    super(router, activatedRoute);
    this.modalTitle = this.translateService.instant('Requests.SignatureDetails.Long');
    this.modalClass = 'modal-lg';
  }

  ngOnInit() {
    this.okButtonClass = 'd-none';
    this.cancelButtonText = this.translateService.instant('Close');
    this.subscription = this.activatedRoute.params.subscribe(async (params) => {
      const documentNumberSeriesItem = params && params.documentNumberSeriesItem;
      if (documentNumberSeriesItem == null) {
        return;
      }
      try {
        this.loadingService.showLoading();
        this.loading = true;
        await this.loadSignActions(documentNumberSeriesItem);
      } catch (err) {
        console.error(err);
        this.lastError = err;
      } finally {
        this.loadingService.hideLoading();
        this.loading = false;
      }
    });
  }

  async loadSignActions(document: number | string) {
    // get sign actions regarding the document
    const signActions = await this.context
      .model('DocumentSignActions')
      .where('documentNumberSeriesItem')
      .equal(document)
      .select('id', 'owner', 'actionStatus', 'reportTemplateRole', 'dateSigned', 'cancelReason')
      .expand('owner', 'reportTemplateRole($expand=reportTemplate, roleType($expand=locale))')
      .orderBy('reportTemplateRole/index asc')
      .take(QueryOptions.All)
      .getItems();
    // if there are none, exit
    if (!(Array.isArray(signActions) && signActions.length > 0)) {
      return;
    }
    // group them by owner
    const actionsByOwner = groupBy(signActions, 'owner.id');
    const latestActions = [];
    for (const [_, actions] of Object.entries(actionsByOwner)) {
      // ensure that actions are always sorted by id desc
      actions.sort((actionOne, actionNext) => (actionOne.id < actionNext.id ? 1 : -1));
      // get latest action
      latestActions.push(actions[0]);
    }
    const completed = [],
      other = [];
    // determine who already signed and who has to sign the document
    for (const action of latestActions) {
      if (action.actionStatus && action.actionStatus.alternateName === 'CompletedActionStatus') {
        completed.push(action);
      } else {
        other.push(action);
      }
    }
    const reportTemplate = signActions[0].reportTemplateRole && signActions[0].reportTemplateRole.reportTemplate;
    // set priority
    this.useSignaturePriority = reportTemplate && reportTemplate.useSignaturePriority;
    // set actions (completed first)
    Array.prototype.push.apply(this.actions, completed);
    Array.prototype.push.apply(this.actions, other);
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  async ok() {
    return this.close();
  }

  async cancel() {
    return this.close();
  }
}
