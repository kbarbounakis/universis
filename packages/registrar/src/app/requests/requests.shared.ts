import {ModuleWithProviders, NgModule, OnInit} from '@angular/core';
import {CommonModule} from '@angular/common';
import {environment} from '../../environments/environment';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {SharedModule} from '@universis/common';
import {FormsModule} from '@angular/forms';
import {RequestsPreviewFormComponent} from './components/requests-preview-form/requests-preview-form.component';
import {StudentsSharedModule} from '../students/students.shared';
import {SettingsSharedModule} from '../settings-shared/settings-shared.module';
import {SettingsService} from '../settings-shared/services/settings.service';
import {FormioModule} from 'angular-formio';
import {MostModule} from '@themost/angular';
import {RequestActionComponent} from './components/request-action/request-action.component';
import {ReportsSharedModule} from '../reports-shared/reports-shared.module';
import {NgxDropzoneModule} from 'ngx-dropzone';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    FormioModule,
    MostModule,
    SharedModule,
    TranslateModule,
    StudentsSharedModule,
    SettingsSharedModule,
    ReportsSharedModule,
    NgxDropzoneModule
  ],
  declarations: [
    RequestsPreviewFormComponent,
      RequestActionComponent,
  ],
  exports: [
    RequestsPreviewFormComponent,
    RequestActionComponent,
  ],
  entryComponents: [
    RequestActionComponent,
  ]
})
export class RequestsSharedModule implements OnInit {

  static forRoot(): ModuleWithProviders {
    return {
      ngModule: RequestsSharedModule,
      providers: [
      ]
    };
  }

  constructor(private _translateService: TranslateService, private _settings: SettingsService) {
    this.ngOnInit().catch(err => {
      console.error('An error occurred while loading requests shared module');
      console.error(err);
    });
  }

  async ngOnInit() {
    // load i18n translations for this module
    const sources = environment.languages.map( language => {
      return import(`./i18n/requests.${language}.json`).then( translations => {
        this._translateService.setTranslation(language, translations, true);
      }).catch( err => {
        console.log(err);
      });
    });
    // await for translations
    await Promise.all(sources);
    this._translateService.onLangChange.subscribe( () => {
      const Requests = this._translateService.instant('Requests');
      this._settings.removeSection('DocumentConfiguration');
      this._settings.removeSection('RequestConfiguration');
      this._settings.addSection({
        name: 'DocumentConfiguration',
        description: Requests.RequestTypes.RequestDocuments,
        longDescription: Requests.RequestTypes.RequestDocumentsDescription,
        category: 'Settings',
        url: '/requests/documents'
      });
      this._settings.addSection({
        name: 'RequestConfiguration',
        description: Requests.RequestTypes.Types,
        longDescription: Requests.RequestTypes.TypesDescription,
        category: 'Settings',
        url: '/requests/types'
      });
   });
  }

}
