
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RequestsRoutingModule } from './requests.routing';
import { RequestsSharedModule } from './requests.shared';
import { RequestsTableComponent } from './components/requests-table/requests-table.component';
import { TablesModule } from '@universis/ngx-tables';
import { TranslateModule } from '@ngx-translate/core';
import { RequestsRootComponent } from './components/requests-root/requests-root.component';
import { RequestsHomeComponent } from './components/requests-home/requests-home.component';
import { RequestsEditComponent } from './components/requests-edit/requests-edit.component';
import {SharedModule} from '@universis/common';
import {FormsModule} from '@angular/forms';
import {ElementsModule} from '../elements/elements.module';
import { ModalModule } from 'ngx-bootstrap';
import { MostModule } from '@themost/angular';
import { BsDatepickerModule } from 'ngx-bootstrap';
import { DatePipe } from '@angular/common';
import { RequestsDocumentsComponent } from './components/requests-documents/requests-documents.component';
import { RequestsDocumentsHomeComponent } from './components/requests-documents-home/requests-documents-home.component';
import { RegistrarSharedModule } from '../registrar-shared/registrar-shared.module';
import {
    ExamPeriodParticipateActionContainer,
    ExamPeriodParticipateActionPreviewComponent
} from './components/exam-period-participate-action/preview/preview.component';
import {ReportsSharedModule} from '../reports-shared/reports-shared.module';
import {
  DocumentActionContainer,
  RequestDocumentActionPreviewComponent
} from './components/request-document-action/preview/preview.component';
import { RequestSidePreviewComponent } from './components/request-side-preview/request-side-preview.component';
import { RequestDescriptionComponent } from './components/request-description/request-description.component';
import {RouterModalModule} from '@universis/common/routing';
import {
  GraduationRequestActionContainer,
  GraduationRequestActionPreviewComponent
} from './components/graduation-request-action/preview/preview.component';
import {AdvancedFormsModule} from '@universis/forms';
import { RulesModule } from '../rules';
import {RequestActionLogsComponent} from './components/request-action-logs/request-action-logs.component';
import {RequestAttachmentsComponent} from './components/request-attachments/request-attachments.component';
import { RequestsThesisActionComponent } from './components/requests-thesis-action/requests-thesis-action.component';
import { DigitalSignatureDetailsComponent } from './components/digital-signature-details/digital-signature-details.component';

@NgModule({
    imports: [
        CommonModule,
        RequestsSharedModule,
        RequestsRoutingModule,
        TablesModule,
        TranslateModule,
        SharedModule,
        FormsModule,
        ElementsModule,
        RegistrarSharedModule,
        ModalModule,
        MostModule,
        BsDatepickerModule.forRoot(),
        ReportsSharedModule,
        RouterModalModule,
        AdvancedFormsModule,
        RulesModule
    ],
  providers: [DatePipe],
  declarations: [
    RequestsHomeComponent,
    RequestsEditComponent,
    RequestsRootComponent,
    RequestsTableComponent,
    RequestsDocumentsComponent,
    RequestsDocumentsHomeComponent,
    ExamPeriodParticipateActionPreviewComponent,
    RequestDocumentActionPreviewComponent,
    DocumentActionContainer,
    RequestSidePreviewComponent,
    RequestDescriptionComponent,
    ExamPeriodParticipateActionContainer,
    GraduationRequestActionContainer,
    GraduationRequestActionPreviewComponent,
    RequestActionLogsComponent,
    RequestAttachmentsComponent,
    RequestsThesisActionComponent,
    DigitalSignatureDetailsComponent
  ],
  exports: [
    ],
  entryComponents: [
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class RequestsModule { }
