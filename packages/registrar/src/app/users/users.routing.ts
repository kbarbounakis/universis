import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {UsersRootComponent} from './components/users-root/users-root.component';
import {UsersHomeComponent} from './components/users-home/users-home.component';
import {UsersTableComponent} from './components/users-table/users-table.component';
import {UsersDashboardComponent} from './components/users-dashboard/users-dashboard.component';
import {UsersDashboardOverviewComponent} from './components/users-dashboard/users-dashboard-overview/users-dashboard-overview.component';
import {UserTableConfigurationResolver, UserTableSearchResolver} from './components/users-table/users-table-config.resolver';
import {AdvancedFormRouterComponent} from '@universis/forms';

const routes: Routes = [
  {
    path: '',
    component: UsersHomeComponent,
    data: {
      title: 'Users Home'
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'list/active'
      },
      {
        path: 'list',
        pathMatch: 'full',
        redirectTo: 'list/index'
      },
      {
        path: 'list/:list',
        component: UsersTableComponent,
        data: {
          title: 'Users'
        },
        resolve: {
          tableConfiguration: UserTableConfigurationResolver,
          searchConfiguration: UserTableSearchResolver
        }
      }
    ]
  },
  {
    path: 'create',
    component: UsersRootComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'new'
      },
      {
        path: 'new',
        component: AdvancedFormRouterComponent,
        data: {

        }
      }
    ],
    resolve: {

    }
  },
  {
    path: ':id',
    component: UsersRootComponent,
    data: {
      title: 'User Home'
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'dashboard'
      },
      {
        path: 'dashboard',
        component: UsersDashboardComponent,
        data: {
          title: 'Users.Dashboard'
        },
        children: [
          {
            path: '',
            pathMatch: 'full',
            redirectTo: 'overview'
          },
          {
            path: 'overview',
            component: UsersDashboardOverviewComponent,
            data: {
              title: 'Users.Overview'
            }
          }
        ]
      },
      {
        path: ':action',
        component: AdvancedFormRouterComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: []
})
export class UsersRoutingModule {
}
