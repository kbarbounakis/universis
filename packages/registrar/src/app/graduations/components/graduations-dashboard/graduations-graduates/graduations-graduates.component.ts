import { Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import * as GRADUATIONS_GRADUATES_LIST_CONFIG from './graduations-graduates.config.json';
import * as GRADUATIONS_GRADUATES_SEARCH_CONFIG from './graduations-graduates.search.json';
import {
  AdvancedRowActionComponent,
  AdvancedTableComponent,
  AdvancedTableConfiguration,
  AdvancedTableDataResult
} from '@universis/ngx-tables';
import {AdvancedSearchFormComponent} from '@universis/ngx-tables';
import { ErrorService, LoadingService, ModalService, ToastService } from '@universis/common';
import { ClientDataQueryable } from '@themost/client';
import { Observable, Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-graduations-graduates',
  templateUrl: './graduations-graduates.component.html'
})
export class GraduationsGraduatesComponent implements OnInit, OnDestroy{

  public recordsTotal: any;
  @ViewChild('graduates') graduates: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  graduationEventId: any = this._activatedRoute.snapshot.params.id;
  private selectedItems: any[] = [];
  @Output() refreshAction: EventEmitter<any> = new EventEmitter<any>();
  private _paramsSubscription: Subscription;
  private _graduationEvent: any;


  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _loadingService: LoadingService,
              private _modalService: ModalService,
              private _errorService: ErrorService,
              private _toastService: ToastService,
              private _translateService: TranslateService,
              private _router: Router

  ) { }

  async ngOnInit() {
    this._paramsSubscription = this._activatedRoute.params.subscribe(params => {
      this.graduationEventId = params.id;
      this.graduates.query = this._context.model('GraduationRequestActions')
      // get requests regarding this event
      .where('graduationEvent').equal(this.graduationEventId)
      // with graduated students
      .and('student/studentStatus/alternateName').equal('graduated')
      // that are completed (e.g the students have been graduated from this event specifically)
      .and('actionStatus/alternateName').equal('CompletedActionStatus')
      .prepare();

      this.graduates.config = AdvancedTableConfiguration.cast(GRADUATIONS_GRADUATES_LIST_CONFIG);
      this.search.form = AdvancedTableConfiguration.cast(GRADUATIONS_GRADUATES_SEARCH_CONFIG);
      this.graduates.ngOnInit();
      this.search.ngOnInit();
    });
  }

    onDataLoad(data: AdvancedTableDataResult) {
      this.recordsTotal = data.recordsTotal;
    }

    async cancelGraduation() {
      try {
        this._loadingService.showLoading();
        const items = await this.getSelectedItems();
        const validationPromises = items.map(async (item) => {
          return {
            id: item.id,
            studentId: item.studentId,
            graduateAction: await this.getCompletedGraduateAction(item.id),
          };
        });
        // filter items with completed graduate actions
        this.selectedItems = (await Promise.all(validationPromises)).filter(
          (item) => !!item.graduateAction
        );
        this._loadingService.hideLoading();
        this._modalService.openModalComponent(AdvancedRowActionComponent, {
          class: 'modal-lg',
          keyboard: false,
          ignoreBackdropClick: true,
          initialState: {
            items: this.selectedItems,
            modalTitle: 'Graduations.ResetToDeclaredAction.Title',
            description: 'Graduations.ResetToDeclaredAction.Description',
            errorMessage:
              'Graduations.ResetToDeclaredAction.CompletedWithErrors',
            refresh: this.refreshAction,
            execute: this.executeCancelGraduation(),
          },
        });
      } catch (err) {
        this._loadingService.hideLoading();
        this._errorService.showError(err, {
          continueLink: '.',
        });
      }
    }

  executeCancelGraduation() {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0,
      };
      this.refreshAction.emit({
        progress: 1,
      });
      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100),
            });
            // get student graduate action
            const graduateAction = item.graduateAction;
            // cancel it
            graduateAction.actionStatus = {
              alternateName: 'CancelledActionStatus'
            };
            // and save
            await this._context.model('StudentGraduateActions').save(graduateAction);
            result.success += 1;
          } catch (err) {
            // log error
            console.error(err);
            result.errors += 1;
          }
        }
      })()
        .then(() => {
          this.graduates.fetch();
          observer.next(result);
        })
        .catch((err) => {
          observer.error(err);
        });
    });
  }

  async getSelectedItems() {
    let items = [];
    if (this.graduates && this.graduates.lastQuery) {
      const lastQuery: ClientDataQueryable = this.graduates.lastQuery;
      if (lastQuery != null) {
        if (this.graduates.smartSelect) {
          // get items
          const selectArguments = [
            'id',
            'student/id as studentId'
          ];
          // query items
          const queryItems = await lastQuery.select
            .apply(lastQuery, selectArguments)
            .take(-1)
            .skip(0)
            .getItems();
          if (this.graduates.unselected && this.graduates.unselected.length) {
            // remove items that have been unselected by the user
            items = queryItems.filter((item) => {
              return (
                this.graduates.unselected.findIndex((x) => {
                  return x.id === item.id;
                }) < 0
              );
            });
          } else {
            items = queryItems;
          }
        } else {
          // get selected items only
          items = this.graduates.selected.map((item) => {
            return {
              id: item.id,
              studentId: item.studentId
            };
          });
        }
      }
    }
    return items;
  }

  getCompletedGraduateAction(initiator: number | string): Promise<any> {
    if (initiator == null) {
      return Promise.resolve();
    }
    return this._context.model('StudentGraduateActions')
      .where('initiator').equal(initiator)
      .and('actionStatus/alternateName').equal('CompletedActionStatus')
      .orderByDescending('id')
      .getItem();
  }

  /**
   *
   * @param {string} method
   */
  async calculatePercentileRanks(method) {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      // filter items with completed graduate actions
      this.selectedItems = items.filter((item) => !!item.id);
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: `Graduations.CalculateRankBy${method}Year.Title`,
          description: `Graduations.CalculateRankBy${method}Year.Description`,
          errorMessage:
            `Graduations.CalculateRankBy${method}Year.CompletedWithErrors`,
          refresh: this.refreshAction,
          execute: this.executeCalculateRanks(method),
        },
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.',
      });
    }
  }

  async calculateClassesPercentileRank() {
    try {
      this._loadingService.showLoading();
      // validate graduation event status
      const graduationEvent = await this._getGraduationEvent();
      if (!(graduationEvent && graduationEvent.eventStatus === 'EventCompleted')) {
        return this._modalService.showInfoDialog(
          this._translateService.instant('Graduations.CalculateClassesPercentileRank.OnlyCompleted.Title'),
          this._translateService.instant('Graduations.CalculateClassesPercentileRank.OnlyCompleted.Message'));
      }
      const items = await this.getSelectedItems();
      // filter items with completed graduate actions
      this.selectedItems = items.filter((item) => !!item.id);
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: `Graduations.CalculateClassesPercentileRank.Title`,
          description: `Graduations.CalculateClassesPercentileRank.Description`,
          errorMessage:
            `Graduations.CalculateClassesPercentileRank.CompletedWithErrors`,
          refresh: this.refreshAction,
          execute: this.executeClassesRankCalculation(),
        },
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.',
      });
    } finally {
      this._loadingService.hideLoading();
    }
  }

  /**
   *
   * @param {string} method
   * @private
   */
  private executeCalculateRanks(method) {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0,
      };
      this.refreshAction.emit({
        progress: 1,
      });
      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100),
            });
            await this._context.model(`GraduationEvents/${this.graduationEventId}/calculatePercentileRanksBy${method}Year`).save([item.id]);
            result.success += 1;
          } catch (err) {
            // log error
            console.error(err);
            result.errors += 1;
          }
        }
      })()
        .then(() => {
          this.graduates.fetch();
          observer.next(result);
        })
        .catch((err) => {
          observer.error(err);
        });
    });
  }

  // this is a resource intensive calculation in the backend
  // send all the ids in one payload in order to save duplicate
  // calculations when many students have sat the same exam.
  private executeClassesRankCalculation() {
    return new Observable((observer) => {
      this.refreshAction.emit({
        progress: 1
      });
      const result = {
        total: 1,
        success: 0,
        errors: 0,
      };
      // handle fake progress with interval
      let progressValue = 5;
      const progressInterval = setInterval(() => {
        progressValue = progressValue + 10 < 100 ? progressValue + 10 : 5;
        this.refreshAction.emit({
          progress: progressValue
        });
      }, 1000);
      // execute promises in series within an async method
      (async () => {
          try {
            let toastData: {
              Title: string,
              Message: string
            };
            // try to find if there exists a calculation action in progress
            const inProgressAction = await this._context.model('PercentileRankActions')
              .where('targetType').equal('GraduationEvent')
              .and('targetIdentifier').equal((this._activatedRoute.snapshot.params && this._activatedRoute.snapshot.params.id).toString())
              .and('rankType/alternateName').equal('coursePercentileRank')
              .and('actionStatus/alternateName').equal('ActiveActionStatus')
              .select('id')
              .getItem();
            if (inProgressAction) {
              toastData = this._translateService.instant('Graduations.CalculateClassesPercentileRank.ActionInProgress');
              this._toastService.show(toastData.Title, toastData.Message, false, 6000);
              return this._router.navigate(['../percentile-rank-results'], {relativeTo: this._activatedRoute});
            }
            const actionResult = await this._context.model(`GraduationEvents/${this.graduationEventId}/calculatePercentileRanks`)
              .save(this.selectedItems.map(x => x.id));
            result.success += this.selectedItems.length;
            // show toast message for already calculated percentile ranks
            if (actionResult && actionResult.type === 'ValidationResult' && actionResult.code === 'ALREADY_CALCULATED') {
              toastData = this._translateService.instant('Graduations.CalculateClassesPercentileRank.AlreadyCalculated');
              return this._toastService.show(toastData.Title, toastData.Message, false, 6000);
            }
            // show toast message for the begining of the async procedure
            toastData = this._translateService.instant('Graduations.CalculateClassesPercentileRank.ProcessHasBegun');
            this._toastService.show(toastData.Title, toastData.Message, false, 6000);
          } catch (err) {
            // log error
            console.error(err);
            result.errors += this.selectedItems.length;
          }
      })()
        .then(() => {
          // stop progress
          if (progressInterval) {
            clearInterval(progressInterval);
          }
          this.graduates.fetch();
          observer.next(result);
        })
        .catch((err) => {
          // stop progress
          if (progressInterval) {
            clearInterval(progressInterval);
          }
          observer.error(err);
        });
    });
  }

  private _getGraduationEvent() {
    // tslint:disable-next-line: triple-equals
    if (this._graduationEvent && this._graduationEvent.id == this.graduationEventId) {
      // note: consider activated route id changes before returning the cached result
      return this._graduationEvent;
    }
    return this._context.model('GraduationEvents')
      .where('id').equal(this.graduationEventId)
      .select('id', 'eventStatus/alternateName as eventStatus')
      .getItem().then((graduationEvent) => {
        // cache graduation event
        this._graduationEvent = graduationEvent;
        return Promise.resolve(graduationEvent);
      }).catch(err => {
        return Promise.reject(err);
      });
  }

  ngOnDestroy(): void {
    if (this._paramsSubscription) {
      this._paramsSubscription.unsubscribe();
    }
  }
}
