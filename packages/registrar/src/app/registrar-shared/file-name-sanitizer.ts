
export class FileNameSanitizer {
    sanitize(name: string) {
        return name.replace(/[`~!@#$%^&*|\?;:'",.<>\{\}\[\]\\\/]/gi, '_');
    }
}