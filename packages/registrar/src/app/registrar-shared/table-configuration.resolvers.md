# TableConfigurationResolver

This resolver may be used for getting table configuration based on routing attributes.

Use `TableConfigurationResolver` in any routing module:

    const routes: Routes = [
        ...
        {
            path: 'list/:list',
            component: StudyProgramsTableComponent,
            data: {
                model: 'StudyPrograms'
            },
            resolve: {
                tableConfiguration: TableConfigurationResolver,
                ...
            }
        }
        ...
    ]

where `TableConfigurationResolver` will use `model` and `list` params to resolve table configuration *.json from 

    `assets/tables/${model}/config.${list}.json`

If `list` param is missing the resolver will try to get the default table configuration which should exist under

    `assets/tables/${model}/config.list.json`

Use `TableConfigurationResolver.get(model: string, list?: string)` to get table configuration by using resolver as a service

    @Component({
        selector: 'app-my-component',
        templateUrl: './my-component.html',
    })
    export class MyComponent implements OnInit {

        constructor(...,
                    private resolver: TableConfigurationResolver) { }

        ngOnInit() {
            this.resolver.get('StudyPrograms').subscribe((tableConfiguration) => {
                // use table configuration
            });
        }
    }

# SearchConfigurationResolver

This resolver may be used for getting search configuration based on routing attributes.

Use `SearchConfigurationResolver` in any routing module:

    const routes: Routes = [
        ...
        {
            path: 'list/:list',
            component: StudyProgramsTableComponent,
            data: {
                model: 'StudyPrograms'
            },
            resolve: {
                searchConfiguration: SearchConfigurationResolver,
                ...
            }
        }
        ...
    ]

where `SearchConfigurationResolver` will use `model` and `list` params to resolve search configuration *.json -which is typically a form- from 

    `assets/tables/${model}/search.${list}.json`

If `list` param is missing resolver will try to get the default search configuration which should exist under

    `assets/tables/${model}/search.list.json`

Use `SearchConfigurationResolver.get(model: string, list?: string)` to get search configuration by using resolver as a service

    @Component({
        selector: 'app-my-component',
        templateUrl: './my-component.html',
    })
    export class MyComponent implements OnInit {

        constructor(...,
                    private resolver: SearchConfigurationResolver) { }

        ngOnInit() {
            this.resolver.get('StudyPrograms').subscribe((searchConfiguration) => {
                // use search configuration
            });
        }
    }

    