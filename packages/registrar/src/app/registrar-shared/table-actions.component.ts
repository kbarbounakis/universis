import { Component, OnInit } from '@angular/core';
import { ActivatedTableService } from '@universis/ngx-tables';

@Component({
    selector: 'app-table-actions',
    template: `
        <div class="btn-group" role="group">
          <button type="button" [translate]="'Tables.Export'" class="btn btn-light universis-btn-export">Export</button>
          <button type="button" class="btn btn-light dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <span class="sr-only">Toggle Dropdown</span>
          </button>
          <div class="dropdown-menu dropdown-menu-right">
            <button [disabled]="activatedTable.activeTable == null" type="button" class="dropdown-item universis-btn-export" [translate]="'Tables.Export'">Export</button>
            <button [disabled]="!(activatedTable.activeTable && activatedTable.activeTable.columnOrdering)" (click)="activatedTable.activeTable.selectColumns()"  type="button" class="dropdown-item">
              <span class="dropdown-item-beta" [translate]="'Tables.SelectColumns'"></span>
            </button>
          </div>
        </div>
    `,
    styles: [`
      .dropdown-item-beta:after {
          content: 'BETA';
          padding-left: 8px;
          font-size: 0.7rem;
          color: var(--danger);
          font-weight: 500;
      }
    `]
  })
  export class TableActionsComponent implements OnInit {
  
  
    constructor(public activatedTable: ActivatedTableService) { }
  
    ngOnInit() {
  
    }
    
  }
  