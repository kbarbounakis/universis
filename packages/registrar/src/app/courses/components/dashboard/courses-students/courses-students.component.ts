import {Component, EventEmitter, Input, OnInit, ViewChild, OnDestroy, Output} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import {Observable, Subscription, combineLatest} from 'rxjs';
import {AdvancedSearchFormComponent} from '@universis/ngx-tables';
import {ActivatedTableService} from '@universis/ngx-tables';
import {
  AdvancedTableComponent,
  AdvancedTableConfiguration,
  AdvancedTableDataResult
} from '@universis/ngx-tables';
import {ConfigurationService, ErrorService, LoadingService, LocalizationSettingsConfiguration, ModalService} from '@universis/common';
import {ClientDataQueryable} from '@themost/client';
import {AdvancedRowActionComponent} from '@universis/ngx-tables';
import { map } from 'rxjs/operators';


@Component({
  selector: 'app-courses-students',
  templateUrl: './courses-students.component.html'
})
export class CoursesStudentsComponent implements OnInit, OnDestroy {

  @ViewChild('students') students: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  public recordsTotal: any;
  private dataSubscription: Subscription;
  public courseId: any;
  private fragmentSubscription: Subscription;
  @Input() reload: EventEmitter<any> = new EventEmitter<any>();
  private subscription: Subscription;
  private selectedItems = [];
  @Output() refreshAction: EventEmitter<any> = new EventEmitter<any>();
  private i18nConfiguration: LocalizationSettingsConfiguration;

  constructor(private _activatedRoute: ActivatedRoute,
              private _activatedTable: ActivatedTableService,
              private _context: AngularDataContext,
              private _loadingService: LoadingService,
              private _modalService: ModalService,
              private _errorService: ErrorService,
              private _configurationService: ConfigurationService
  ) {
    // get localization settings configuration
    this.i18nConfiguration = this._configurationService.settings && this._configurationService.settings.i18n;
  }

  async ngOnInit() {
    this.subscription = combineLatest(
      this._activatedRoute.params,
      this._activatedRoute.data
      ).pipe(
        map(([params, data]) => ({params, data}))
      ).subscribe(async (results) => {
      this.courseId = results.params.id;
      this._activatedTable.activeTable = this.students;
      this.students.query = this._context.model('StudentCourses')
        .asQueryable()
        .where('course').equal(results.params.id)
        .prepare();
      this.students.config = AdvancedTableConfiguration.cast(results.data.tableConfiguration);
      this.students.fetch();
      // declare model for advance search filter criteria
      this.students.config.model = 'StudentCourses';
      if (results.data.searchConfiguration) {
        this.search.form =  Object.assign(results.data.searchConfiguration,
          { course: this.courseId,
            department: this._activatedRoute.snapshot.data.department});
        this.search.ngOnInit();
      }
    });
    this.fragmentSubscription = this._activatedRoute.fragment.subscribe(fragment => {
      if (fragment && fragment === 'reload') {
        this.students.fetch(true);
      }
    });
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }

  async changeCourseActiveAction() {
    this.changeCourseAction(true, 'Courses.ChangeCourseAttributesAction.Description.Active')
  }
  async changeCourseAllAction() {
    this.changeCourseAction(false, 'Courses.ChangeCourseAttributesAction.Description.All')
  }

  /***
   * Change student courses attributes
   */
  async changeCourseAction(onlyActive: boolean, formDescription: string) {
    try {
      this._loadingService.showLoading();
      const activeFilter = (item) => {
        return ['active', 'suspended'].includes(item.status);
      }
      let items = await this.getSelectedItems();
      if (onlyActive) {
        items = items.filter(activeFilter);
      }
      this.selectedItems = items;
      let availableLocales = [];
      if (Array.isArray(this.i18nConfiguration
          && this.i18nConfiguration.locales)
          && this.i18nConfiguration.defaultLocale) {
            availableLocales = this.i18nConfiguration.locales
                  // exclude default application locale, as defined
                  .filter((locale: string) => locale !== this.i18nConfiguration.defaultLocale)
                  .map((inLanguage: string) => {
                    return { inLanguage };
                  });
      }
      const itemsAllActive = onlyActive || this.selectedItems.every(activeFilter);
      const formName = (!this.selectedItems || this.selectedItems.length == 0) ? null :
        itemsAllActive ? 'StudentCourses/changeAttributes' : 'StudentCourses/changeAttributesGraduated'
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          formTemplate: formName, 
          modalTitle: 'Courses.ChangeCourseAttributesAction.Title',
          description: formDescription,
          refresh: this.refreshAction,
          execute: this.executeChangeCourseAttributes(),
          data: {
            availableLocales
          }
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  executeChangeCourseAttributes() {
    return new Observable((observer) => {
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0,
        additionalMessage: ''
      };
      // get values from modal component
      const component = this._modalService.modalRef.content as AdvancedRowActionComponent;
      const data = component.formComponent.form.formio.data;
      // filter empty values
      Object.keys(data).forEach((key: string) => {
        // handle locales seperately
        if (key === 'locales' && Array.isArray(data[key])) {
          data[key] = data[key].filter((item: {
            inLanguage: string,
            courseTitle: string,
          }) => !!item.courseTitle);
          if (data[key].length === 0) {
            delete data[key];
          }
          return;
        }
        if (data[key] == null || data[key] === '' ||  (typeof data[key] === 'object' && data[key].id == null)) {
          delete data[key];
        }
      });
      if (Object.keys(data).length === 0) {
        this.selectedItems = [];
        result.errors = result.total;
        return observer.next(result);
      }
      this.refreshAction.emit({
        progress: 1
      });
      const items = {
        students: this.selectedItems,
        attributes: data
      };
      this._context.model(`Courses/${this.courseId}/updateStudents`).save(items).then(() => {
        result.success = result.total;
        observer.next(result);
      }).catch((err) => {
        this.selectedItems = [];
        result.errors = result.total;
        result.additionalMessage = err.message;
        return observer.next(result);
      });
    });
  }

  async getSelectedItems() {
    let items = [];
    if (this.students && this.students.lastQuery) {
      const lastQuery: ClientDataQueryable = this.students.lastQuery;
      // search for document attributes (if table has published column)

      if (lastQuery != null) {
        if (this.students.smartSelect) {
          // get items
          const selectArguments = ['student/id as id', 'student/studentStatus/alternateName as status'];

          // query items
          const queryItems = await lastQuery.select.apply(lastQuery, selectArguments)
            .take(-1)
            .skip(0)
            .getItems();
          if (this.students.unselected && this.students.unselected.length) {
            // remove items that have been unselected by the user
            items = queryItems.filter(item => {
              return this.students.unselected.findIndex((x) => {
                return x.id === item.id;
              }) < 0;
            });
          } else {
            items = queryItems;
          }
        } else {
          // get selected items only
          items = this.students.selected.map((item) => {
            return {
              id: item.id,
              status: item.studentStatus
            };
          });
        }
      }
    }
    return items;
  }

}
