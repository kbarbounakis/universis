import { NgModule, OnInit, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MostModule } from '@themost/angular';
import { TranslateService, TranslateModule } from '@ngx-translate/core';
import { environment } from '../../environments/environment';
import { ReportService } from './services/report.service';
import { SettingsSharedModule } from '../settings-shared/settings-shared.module';
import { SettingsService } from '../settings-shared/services/settings.service';
import { SharedModule } from '@universis/common';
import { SelectReportComponent } from './components/select-report/select-report.component';
import { FormsModule } from '@angular/forms';
import { TablesModule } from '@universis/ngx-tables';
import { FormioModule } from 'angular-formio';
import {NgxExtendedPdfViewerModule} from 'ngx-extended-pdf-viewer';
import { RegistrarSharedModule } from '../registrar-shared/registrar-shared.module';
import { ItemDocumentsComponent } from './components/item-documents/item-documents.component';
import {DocumentDownloadComponent} from './components/document-download/document-download.component';
import {NgxDropzoneModule} from 'ngx-dropzone';
import { PrintReportComponent } from './components/print-report/print-report.component';
import { DocumentDownloadRoutedComponent } from './components/document-download-routed/document-download-routed.component';
import { NgxSignerModule } from '@universis/ngx-signer';
import { DocumentPublisherComponent } from './components/item-documents/document-publisher.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    FormioModule,
    MostModule,
    SharedModule,
    TablesModule,
    TranslateModule,
    SettingsSharedModule,
    NgxExtendedPdfViewerModule,
    RegistrarSharedModule,
    NgxDropzoneModule,
    NgxSignerModule
  ],
  declarations: [
    SelectReportComponent,
    ItemDocumentsComponent,
    DocumentDownloadComponent,
    PrintReportComponent,
    DocumentDownloadRoutedComponent,
    DocumentPublisherComponent
  ],
  exports: [
    SelectReportComponent,
    ItemDocumentsComponent,
    DocumentDownloadComponent,
    PrintReportComponent,
    DocumentDownloadRoutedComponent,
    DocumentPublisherComponent
  ],
  entryComponents: [
  ]
})
export class ReportsSharedModule implements OnInit {


    static forRoot(): ModuleWithProviders {
        return {
            ngModule: ReportsSharedModule,
            providers: [
              ReportService
            ]
        };
        }
  constructor(private _translateService: TranslateService,
  private _settings: SettingsService) {
      this.ngOnInit().catch( err => {
        console.error('An error occurred while loading ReportsSharedModule');
        console.error(err);
      });
  }

  async ngOnInit() {
    // load i18n translations for this module
    const sources = environment.languages.map( language => {
      return import(`./i18n/reports.${language}.json`).then( translations => {
        this._translateService.setTranslation(language, translations, true);
      }).catch( err => {
        console.log(err);
      });
    });
    // await for translations
    await Promise.all(sources);
    // add extra sections for managing reports and report categories
    // subscribe for language change
    this._translateService.onLangChange.subscribe( () => {
      const Reports = this._translateService.instant('Reports');
      this._settings.addSection({
        name: 'ReportTemplate',
        description: Reports.Lists.ReportTemplate.Description,
        longDescription: Reports.Lists.ReportTemplate.LongDescription,
        category: 'Reports',
        url: '/reports/configuration/templates'
      }, {
        name: 'ReportCategory',
        description: Reports.Lists.ReportCategory.Description,
        longDescription: Reports.Lists.ReportCategory.LongDescription,
        category: 'Reports',
        url: '/reports/configuration/categories'
      });
    });
  }
}
