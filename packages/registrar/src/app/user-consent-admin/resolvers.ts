import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { ActiveFieldGroupService } from './active-field-group.service';
import { Observable } from 'rxjs';
import { first, map } from 'rxjs/operators';
import { ActivatedUser, AuthGuard } from '@universis/common';

@Injectable()
export class ParentFieldGroupResolver implements Resolve<any> {
  constructor(private activeGroup: ActiveFieldGroupService) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<any> {
    return this.activeGroup.group.pipe(first());
  }
}

@Injectable()
export class ReadonlyAccessResolver implements Resolve<any> {
  constructor(private activateUser: ActivatedUser,
    private authGuard: AuthGuard) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> {
    return this.activateUser.user.pipe(first(), map((user: any) => {
      const locationPermission = this.authGuard.canActivateLocation('/services/consents', user);
      const mask = locationPermission && locationPermission.mask;
      return (mask & 1) === 0;
    }));
  }
}