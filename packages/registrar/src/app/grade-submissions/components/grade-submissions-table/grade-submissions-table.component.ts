import {Component, OnInit, ViewChild, OnDestroy, Output, EventEmitter} from '@angular/core';
import { AdvancedTableComponent, AdvancedTableDataResult } from '@universis/ngx-tables';
import { Router, ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import {Observable, Subscription, combineLatest} from 'rxjs';
import { AdvancedSearchFormComponent } from '@universis/ngx-tables';
import {AdvancedTableSearchComponent} from '@universis/ngx-tables';
import {ActivatedTableService} from '@universis/ngx-tables';
import {TranslateService} from '@ngx-translate/core';
import {ErrorService, LoadingService, ModalService} from '@universis/common';
import { ClientDataQueryable } from '@themost/client';
import { RequestActionComponent } from '../../../requests/components/request-action/request-action.component';
import { sortBy } from 'lodash';

@Component({
  selector: 'app-grade-submissions-table',
  templateUrl: './grade-submissions-table.component.html',
})
export class GradeSubmissionsTableComponent implements OnInit, OnDestroy {

  private dataSubscription: Subscription;
  public recordsTotal: any;
  private selectedItems = [];
  private takeSize = 100;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  @ViewChild('table') table: AdvancedTableComponent;
  @ViewChild('advancedSearch') advancedSearch: AdvancedTableSearchComponent;
  @Output() refreshAction: EventEmitter<any> = new EventEmitter<any>();

  constructor(private _router: Router,
              private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _translateService: TranslateService,
              private _modalService: ModalService,
              private _loadingService: LoadingService,
              private _errorService: ErrorService,
              private _activatedTable: ActivatedTableService) {
  }

  ngOnInit() {
    this.dataSubscription = this._activatedRoute.data.subscribe( data => {
      this._activatedTable.activeTable = this.table;
      // set search form
      if (data.searchConfiguration) {
        this.search.form = data.searchConfiguration;
        this.search.ngOnInit();
      }
      // set table config and recall data
      if (data.tableConfiguration) {
        // set config
        this.table.config = data.tableConfiguration;
        // reset search text
        this.advancedSearch.text = null;
        // reset table
        this.table.reset(false);
      }
    });
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }

  onSearchKeyDown(event: any) {
    if (event.keyCode === 13) {
      this.table.search((<HTMLInputElement>event.target).value);
      return false;
    }
  }

  onSelectBoxChange(event: any) {
    // get value
    const value = parseInt((<HTMLSelectElement>event.target).value, 10);
    if (isNaN(value)) {
      this._router.navigate(
        [],
        {
          relativeTo: this._activatedRoute,
          queryParams: { $filter: '' },
          queryParamsHandling: 'merge'
        }).then(() => {
          this.table.fetch();
        });
      return;
    }
    this._router.navigate(
      [],
      {
        relativeTo: this._activatedRoute,
        queryParams: { $filter: `status eq ${value}` },
        queryParamsHandling: 'merge'
      }).then(() => {
        this.table.fetch();
      });
  }

  async getSelectedItems() {
    let items = [];
    if (this.table && this.table.lastQuery) {
      const lastQuery: ClientDataQueryable = this.table.lastQuery;
      if (lastQuery != null) {
        if (this.table.smartSelect) {
          // get items
          const selectArguments = ['id', 'actionStatus/alternateName as actionStatus', 'object/course/name as title',
            'object/examPeriod/name as examPeriod', 'dateCreated'];
          // query items
          const queryItems = await lastQuery.select.apply(lastQuery, selectArguments)
            .take(-1)
            .skip(0)
            .getItems();
          if (this.table.unselected && this.table.unselected.length) {
            // remove items that have been unselected by the user
            items = queryItems.filter( item => {
              return this.table.unselected.findIndex( (x) => {
                return x.id === item.id;
              }) < 0;
            });
          } else {
            items = queryItems;
          }
        } else {
          // get selected items only
          items = this.table.selected.map( (item) => {
            return {
              id: item.id,
              actionStatus: item.actionStatus,
              title: item.title,
              examPeriod: item.examPeriod,
              dateCreated: item.dateCreated
            };
          });
        }
      }
    }
    return items;
  }

  async approveAction() {
    try {
      // show loading
      this._loadingService.showLoading();
      // get selected items
      const items = await this.getSelectedItems();
      // filter out any but active status items
      this.selectedItems = items.filter(examDocumentUploadAction => {
        return examDocumentUploadAction.actionStatus === 'ActiveActionStatus';
      });
      // sort selected items by id
      this.selectedItems = sortBy(this.selectedItems, ['id']);
      // hide loading
      this._loadingService.hideLoading();
      // open approval modal
      this._modalService.openModalComponent(RequestActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'GradeSubmissions.Edit.ApproveAction.Title',
          description: 'GradeSubmissions.Edit.ApproveAction.Description',
          refresh: this.refreshAction,
          execute: this.executeApproveAction()
        }
      });
    } catch (err) {
      // hide loading
      this._loadingService.hideLoading();
      // show error
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  executeApproveAction() {
    return new Observable((observer) => {
      this.refreshAction.emit({
        progress: 1
      });
      const total = this.selectedItems.length;
      const result = {
        total: total,
        success: 0,
        errors: 0
      };
      // execute promises in series within an async method
      (async () => {
        for (let i = 0; i < this.selectedItems.length; i++) {
          const item = this.selectedItems[i];
          const res: any = await this._executeApproveActionOne(item, i);
          result.success += res.success;
          result.errors += 1 - res.success;
        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  private _executeApproveActionOne(item, index) {
    const total = this.selectedItems.length;
    return new Promise((resolve, reject) => {
      this._context.model(`ExamDocumentUploadActions/${item.id}/approve`).save(null).then(() => {
        setTimeout(() => {
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            return this.table.fetchOne({
              id: item.id
            }).then(() => {
              return resolve({
                success: 1
              });
            }).catch((err) => {
              console.log(err);
              return resolve({
                success: 1,
                error: err
              });
            });
        }, 500);
      }).catch(err => {
          return resolve({
            success: 0,
            error: err
          });
      });
    });
  }
}
