import {Component, ViewEncapsulation} from '@angular/core';

@Component({
    selector: 'app-router-modal-simple',
    templateUrl: './router-modal-simple.component.html',
    styles: [`
        .tab-pane .card {
            box-shadow: none;
          }
        .tab-pane .card .hljs {
            background: #FFF;
        }
    `],
    encapsulation: ViewEncapsulation.None
})
export class RouterModalSimpleComponent  {
}
